/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.gdx;

public class NullPolling implements Polling {
    @Override
    public void poll() {
    }
}