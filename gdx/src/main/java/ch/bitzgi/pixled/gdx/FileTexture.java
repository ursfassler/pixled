/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.gdx;

import com.badlogic.gdx.graphics.Texture;

import ch.bitzgi.pixled.widget.FileImage;

class FileTexture implements TextureRenderer {
    final private FileImage image;

    public FileTexture(FileImage image) {
        this.image = image;
    }

    @Override
    public Texture render() {
        return createTexture(image.getImage());
    }

    private Texture createTexture(String imageName) {
        return new Texture(imageName + ".png");
    }

}