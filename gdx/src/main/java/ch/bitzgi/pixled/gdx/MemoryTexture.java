/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.gdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;

import ch.bitzgi.pixled.image.Color;
import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.widget.MemoryImage;

class MemoryTexture implements TextureRenderer {
    final private MemoryImage image;

    public MemoryTexture(MemoryImage image) {
        this.image = image;
    }

    @Override
    public Texture render() {
        return createTexture(image.getImage());
    }

    private Texture createTexture(Image image) {
        Pixmap led = new Pixmap(Gdx.files.internal("led.png"));

        int pw = led.getWidth();
        int ph = led.getHeight();
        Pixmap pixmap = new Pixmap((image.width + 1) * pw, (image.height + 1) * ph, Format.RGB888);

        for (int y = 0; y < image.height; y++) {
            for (int x = 0; x < image.width; x++) {
                Color color = image.getPixel(x, y);
                int rawColor = (color.value << 8) | 0x000000ff;
                pixmap.setColor(rawColor);
                pixmap.fillRectangle(x * pw + pw / 2, y * ph + ph / 2, pw, ph);

                pixmap.drawPixmap(led, x * pw + pw / 2, y * ph + ph / 2);
            }
        }
        led.dispose();

        Texture texture = new Texture(pixmap);
        pixmap.dispose();

        return texture;
    }

}