/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.gdx;

import java.util.List;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import ch.bitzgi.pixled.Pixled;
import ch.bitzgi.pixled.PixledFactory;
import ch.bitzgi.pixled.bluetooth.Adapter;
import ch.bitzgi.pixled.gdx.filesystem.GdxDirectory;
import ch.bitzgi.pixled.gdx.image.GdxImageLoader;
import ch.bitzgi.pixled.image.ImageLoader;
import ch.bitzgi.pixled.widget.Button;
import ch.bitzgi.pixled.widget.FileImage;
import ch.bitzgi.pixled.widget.MemoryImage;
import ch.bitzgi.pixled.widget.PixletButton;
import ch.bitzgi.pixled.widget.ScreenImage;
import ch.bitzgi.pixled.widget.SystemButton;

public class GdxMain extends ApplicationAdapter {
    private static final int SPACING = 21;
    private Pixled pixled;
    private final Polling earlyPolling;
    private final Polling latePolling;
    private final Color background = Color.DARK_GRAY;
    private Stage stage;
    private ScrollPane scroll;
    private Table main;
    private Table systemControl;
    private Table contentControl;
    private Adapter adapter;
    private ScreenViewport viewport;

    public GdxMain(Polling earlyPolling, Polling latePolling, Adapter adapter) {
        this.earlyPolling = earlyPolling;
        this.latePolling = latePolling;
        this.adapter = adapter;
    }

    @Override
    public void create() {
        GdxDirectory directory = new GdxDirectory();
        ImageLoader loader = new GdxImageLoader();
        pixled = PixledFactory.produce(adapter, directory, loader);

        pixled.boot();

        Gdx.gl.glClearColor(background.r, background.g, background.b, background.a);

        int w = Gdx.graphics.getWidth();
        int h = Gdx.graphics.getHeight();
        viewport = new ScreenViewport();
        stage = new Stage(viewport);

        main = new Table();
        main.setFillParent(true);
        stage.addActor(main);

        systemControl = new Table();
        addSystem(pixled.getSystem(), systemControl);
        main.add(systemControl).space(SPACING);
        main.row();

        contentControl = new Table();
        scroll = new ScrollPane(contentControl);
        main.add(scroll);

//        Gdx.gl.glViewport(0, 0, w, h);

        addContentButton();

//        main.setDebug(true, true);
        Gdx.input.setInputProcessor(stage);

        stage.draw(); // needed that the size of stage and children is defined
        resize(w, h);
    }

    private void addContentButton() {
        updateContentButtons();
    }

    private void updateContentButtons() {
        contentControl.clearChildren();

        addPixlet(pixled.getContent(), contentControl);
        contentControl.row();
    }

    private void addSystem(List<SystemButton> buttons, Table table) {
        for (SystemButton button : buttons) {
            ButtonAdapter gdximg = createButtonAdapter(button);
            table.add(gdximg.getGdxButton()).size(66, 66).space(SPACING);
        }
    }

    private void addPixlet(List<PixletButton> buttons, Table table) {
        int count = 0;
        for (PixletButton button : buttons) {
            ButtonAdapter gdximg = createButtonAdapter(button);
            table.add(gdximg.getGdxButton()).space(SPACING);

            count = (count + 1) % 3;
            if (count == 0) {
                table.row();
            }
        }
    }

    private ButtonAdapter createButtonAdapter(SystemButton button) {
        FileImage image = button.getImage();
        TextureRenderer renderer = new FileTexture(image);
        return createButtonAdapter(button.getButton(), image, renderer);
    }

    private ButtonAdapter createButtonAdapter(PixletButton button) {
        MemoryImage image = button.getImage();
        TextureRenderer renderer = new MemoryTexture(image);
        return createButtonAdapter(button.getButton(), image, renderer);
    }

    private ButtonAdapter createButtonAdapter(Button button, ScreenImage image, TextureRenderer renderer) {
        ButtonImageAdapter adapter = new ButtonImageAdapter(renderer);
        image.setListener(adapter);
        adapter.imageChanged();
        ButtonImageAdapter imageAdapter = adapter;
        return new ButtonAdapter(button, imageAdapter.getStyle());
    }

    @Override
    public void resize(int width, int height) {
        double contentSize = contentControl.getWidth();
        double wscale = round(contentSize / width);
        double hscale = round(contentSize / height);
        double scale = Math.max(wscale, hscale);
        viewport.setUnitsPerPixel((float) scale);
        stage.getViewport().update(width, height, true);
    }

    private double round(double scale) {
        if (scale >= 1) {
            scale = Math.ceil(scale);
        } else {
            scale = 1 / Math.floor(1 / scale);
        }
        return scale;
    }

    @Override
    public void render() {
        earlyPolling.poll();
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
        latePolling.poll();
    }

    public float getScrollPercentage() {
        return scroll.getVisualScrollPercentY();
    }

    public void setScrollPercentage(float value) {
        scroll.setScrollPercentY(value);
    }
}
