/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.gdx;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import ch.bitzgi.pixled.widget.Button;
import ch.bitzgi.pixled.widget.ButtonStateListener;

class ButtonAdapter extends ChangeListener implements ButtonStateListener {
    final private com.badlogic.gdx.scenes.scene2d.ui.Button gdx;
    final private Button pixled;

    ButtonAdapter(Button pixled, ButtonStyle style) {
        this.pixled = pixled;

        gdx = new com.badlogic.gdx.scenes.scene2d.ui.Button(style);
        gdx.addListener(this);
        pixled.setListener(this);
        enabledChanged();
    }

    public com.badlogic.gdx.scenes.scene2d.ui.Button getGdxButton() {
        return gdx;
    }

    @Override
    public void changed(ChangeEvent event, Actor actor) {
        pixled.click();
    }

    @Override
    public void enabledChanged() {
        boolean disabled = !pixled.getEnabled();
        gdx.setDisabled(disabled);
    }

}
