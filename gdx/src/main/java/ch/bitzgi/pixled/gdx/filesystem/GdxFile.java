/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.gdx.filesystem;

import com.badlogic.gdx.files.FileHandle;

import ch.bitzgi.pixled.filesystem.Directory;
import ch.bitzgi.pixled.filesystem.File;

class GdxFile implements File {
    private final FileHandle handle;

    GdxFile(FileHandle handle) {
        this.handle = handle;
    }

    @Override
    public Directory parent() {
        return new GdxDirectory(handle.parent());
    }

    @Override
    public String name() {
        return handle.name();
    }

}