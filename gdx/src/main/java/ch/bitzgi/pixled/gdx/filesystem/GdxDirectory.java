/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.gdx.filesystem;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import ch.bitzgi.pixled.filesystem.Directory;
import ch.bitzgi.pixled.filesystem.File;

public class GdxDirectory implements Directory {
    private final FileHandle current;

    public GdxDirectory(FileHandle current) {
        this.current = current;
    }

    public GdxDirectory() {
        boolean isAndroid = (Gdx.app.getType() == ApplicationType.Android);
        String root = isAndroid ? "" : ".";
        this.current = Gdx.files.internal(root);
    }

    @Override
    public Directory parent() {
        return new GdxDirectory(current.parent());
    }

    @Override
    public Set<File> listFiles() {
        Set<File> result = new HashSet<File>();

        for (FileHandle handle : list()) {
            if (!handle.isDirectory()) {
                result.add(new GdxFile(handle));
            }
        }

        return result;
    }

    @Override
    public Set<? extends Directory> listDirectories() {
        Set<Directory> result = new HashSet<Directory>();

        for (FileHandle handle : list()) {
            if (handle.isDirectory()) {
                result.add(new GdxDirectory(handle));
            }
        }

        return result;
    }

    private Set<FileHandle> list() {
        Set<FileHandle> result = new HashSet<FileHandle>();

        FileHandle[] list = current.list();
        for (FileHandle handle : list) {
            // Workaround for top level internal file listing on Android
            if ((handle.type() == Files.FileType.Internal) && (handle.path().startsWith("/"))) {
                handle = Gdx.files.internal(handle.path().substring(1));
            }
            result.add(handle);
        }

        return result;
    }

    @Override
    public String name() {
        return current.name();
    }

}