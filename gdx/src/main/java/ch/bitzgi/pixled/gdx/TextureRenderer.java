/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.gdx;

import com.badlogic.gdx.graphics.Texture;

interface TextureRenderer {
    Texture render();
}