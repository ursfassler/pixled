/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.gdx.image;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;

import ch.bitzgi.pixled.filesystem.File;
import ch.bitzgi.pixled.filesystem.FileSystem;
import ch.bitzgi.pixled.image.Color;
import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.image.ImageLoader;

public class GdxImageLoader implements ImageLoader {

    @Override
    public Image load(File file) {
        FileHandle handle = Gdx.files.internal(FileSystem.path(file));
        Pixmap pixmap = new Pixmap(handle);
        return convert(pixmap);
    }

    private static Image convert(Pixmap rawimage) {
        int width = rawimage.getWidth();
        int height = rawimage.getHeight();

        List<Color> pixels = new ArrayList<Color>();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int rgb = rawimage.getPixel(x, y);
                pixels.add(convert(rgb));
            }
        }

        return new Image(width, height, pixels);
    }

    private static Color convert(int rgb) {
        return new Color(rgb >> 8);
    }

    @Override
    public List<String> supportedExtensions() {
        List<String> result = new ArrayList<String>();
        result.add(".png");
        result.add(".jpg");
        return result;
    }

}