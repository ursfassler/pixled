/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.gdx;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import ch.bitzgi.pixled.widget.ImageListener;

class ButtonImageAdapter implements ImageListener {
    final private ButtonStyle style = new ButtonStyle();
    final private TextureRenderer renderer;

    ButtonImageAdapter(TextureRenderer renderer) {
        this.renderer = renderer;
    }

    @Override
    public void imageChanged() {
        Texture texture = renderer.render();
        setButtonImage(texture);
    }

    private void setButtonImage(Texture texture) {
        style.up = new TextureRegionDrawable(new TextureRegion(texture));
    }

    public ButtonStyle getStyle() {
        return style;
    }

}