/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.desktop;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import ch.bitzgi.pixled.bluetooth.Device;
import ch.bitzgi.pixled.gdx.GdxMain;
import ch.bitzgi.pixled.gdx.NullPolling;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Pixled";
        config.width = 480;
        config.height = 764;
        List<Device> devices = new ArrayList<Device>();
        devices.add(new SimulatedDevice("AuraBox"));
        SimulatedAdapter adapter = new SimulatedAdapter(devices);
        GdxMain main = new GdxMain(adapter, new NullPolling(), adapter);
        new LwjglApplication(main, config);
    }

}
