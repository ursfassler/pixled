/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.desktop;

import ch.bitzgi.pixled.bluetooth.Device;

class SimulatedDevice implements Device {
    final private String name;

    public SimulatedDevice(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}