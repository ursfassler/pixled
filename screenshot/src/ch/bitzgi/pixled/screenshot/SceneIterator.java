/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.screenshot;

import java.util.Iterator;

class SceneIterator {
    private final Iterator<ScreenshotScene> screenshotIterator;
    private ScreenshotScene current = null;

    public SceneIterator(Iterator<ScreenshotScene> screenshotIterator) {
        this.screenshotIterator = screenshotIterator;
        next();
    }

    public boolean isValid() {
        return current != null;
    }

    public void next() {
        current = screenshotIterator.hasNext() ? screenshotIterator.next() : null;
    }

    public ScreenshotScene current() {
        return current;
    }

}