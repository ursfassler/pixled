/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.screenshot;

import java.util.ArrayList;
import java.util.List;

class ScreenshotScenes {

    static public List<ScreenshotScene> get() {
        List<ScreenshotScene> result = new ArrayList<ScreenshotScene>();

        result.add(new ScreenshotScene(540, 960, 0.00f, "portrait 0"));
        result.add(new ScreenshotScene(540, 960, 0.75f, "portrait 75"));
        result.add(new ScreenshotScene(960, 540, 0.25f, "landscape 25"));
        result.add(new ScreenshotScene(960, 540, 1.00f, "landscape 100"));

        return result;
    }

}