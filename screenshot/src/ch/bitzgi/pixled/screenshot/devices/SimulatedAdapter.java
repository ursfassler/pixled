/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.screenshot.devices;

import java.util.List;

import ch.bitzgi.pixled.bluetooth.Adapter;
import ch.bitzgi.pixled.bluetooth.ConnectionListener;
import ch.bitzgi.pixled.bluetooth.Device;
import ch.bitzgi.pixled.gdx.Polling;

public class SimulatedAdapter implements Adapter, Polling {
    private final List<Device> devices;
    private Device connectedDevice;
    private ConnectionListener listener;
    private boolean isConnecting = false;

    public SimulatedAdapter(List<Device> devices) {
        this.devices = devices;
    }

    @Override
    public void connectTo(Device device, ConnectionListener listener) {
        this.connectedDevice = device;
        this.listener = listener;
        isConnecting = true;
    }

    @Override
    public List<Device> getDevices() {
        return devices;
    }

    @Override
    public void send(List<Byte> data) {
        System.out.print("send");
        for (byte sym : data) {
            System.out.print(String.format(" %02X", sym));
        }
        System.out.println();
    }

    @Override
    public void poll() {
        if (isConnecting) {
            isConnecting = false;
            listener.connected(connectedDevice);
        }
    }

}