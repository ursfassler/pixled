/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.screenshot;

class ScreenshotScene {
    public final int width;
    public final int height;
    public final float scroll;
    public final String name;

    public ScreenshotScene(int width, int height, float scroll, String name) {
        this.width = width;
        this.height = height;
        this.scroll = scroll;
        this.name = name;
    }

}