/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.screenshot;

import com.badlogic.gdx.Gdx;

import ch.bitzgi.pixled.gdx.GdxMain;
import ch.bitzgi.pixled.gdx.Polling;

class AutoScreenshoter implements Polling {
    private final SceneIterator scene;
    private GdxMain gdxMain = null;

    public AutoScreenshoter(SceneIterator scene) {
        this.scene = scene;
    }

    @Override
    public void poll() {
        if (scene.isValid()) {
            ScreenshotScene current = scene.current();
            if (sceneIsArranged(current)) {
                Screenshot.saveScreenshot(current.name);
                scene.next();
            } else {
                arrangeScene(current);
            }
        } else {
            Gdx.app.exit();
        }
    }

    private void arrangeScene(ScreenshotScene current) {
        Gdx.graphics.setWindowedMode(current.width, current.height);
        gdxMain.setScrollPercentage(current.scroll);
    }

    private boolean sceneIsArranged(ScreenshotScene current) {
        boolean matchWidth = Gdx.graphics.getWidth() == current.width;
        boolean matchHeight = Gdx.graphics.getHeight() == scene.current().height;
        boolean matchScroll = Math.abs(gdxMain.getScrollPercentage() - scene.current().scroll) <= 0.01;
        boolean sceneIsArranged = matchWidth && matchHeight && matchScroll;
        return sceneIsArranged;
    }

    public void setGdxMain(GdxMain gdxMain) {
        this.gdxMain = gdxMain;
    }

}