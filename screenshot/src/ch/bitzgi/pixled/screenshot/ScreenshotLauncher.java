/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.screenshot;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import ch.bitzgi.pixled.bluetooth.Device;
import ch.bitzgi.pixled.gdx.GdxMain;
import ch.bitzgi.pixled.screenshot.devices.SimulatedAdapter;
import ch.bitzgi.pixled.screenshot.devices.SimulatedDevice;

public class ScreenshotLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Pixled Screenshot";
        List<Device> devices = new ArrayList<Device>();
        devices.add(new SimulatedDevice("AuraBox"));
        SimulatedAdapter adapter = new SimulatedAdapter(devices);

        AutoScreenshoter screenshoter = new AutoScreenshoter(new SceneIterator(ScreenshotScenes.get().iterator()));
        GdxMain main = new GdxMain(adapter, screenshoter, adapter);
        screenshoter.setGdxMain(main);

        new LwjglApplication(main, config);
    }

}
