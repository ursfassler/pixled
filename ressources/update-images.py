#!/usr/bin/python3

#
# Copyright 2017 Urs Fässler
# SPDX-License-Identifier: GPL-3.0-only
#


import subprocess
import argparse
import os


parser = argparse.ArgumentParser(description="generate png from the svg")
args = parser.parse_args()

path="."

files=[]
for file in os.listdir(path):
    if file.endswith(".svg"):
        files.append(file)

for file in files:
    source = os.path.join(path, file)
    destination = file[:-4] + ".png"
    destination = os.path.join(path + "/../android/assets/", destination)
    print(source + " -> " + destination)

    call = ["inkscape", "--file=" + source, "--export-png=" + destination, "--export-area-page", "--export-dpi=96" ]
    subprocess.check_call(call)

