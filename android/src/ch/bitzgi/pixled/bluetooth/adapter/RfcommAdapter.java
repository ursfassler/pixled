/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.bluetooth.adapter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.bitzgi.android.bluetooth.spp.Output;
import ch.bitzgi.android.bluetooth.spp.Supervisor;
import ch.bitzgi.pixled.bluetooth.Adapter;
import ch.bitzgi.pixled.bluetooth.ConnectionListener;
import ch.bitzgi.pixled.bluetooth.Device;
import ch.bitzgi.pixled.gdx.Polling;

public class RfcommAdapter implements Adapter, Polling, Output {
    private final Supervisor supervisor = new Supervisor(this);
    private RfcommDevice connected;
    private ConnectionListener listener;

    @Override
    public void connectTo(Device device, ConnectionListener listener) {
        this.listener = listener;
        connected = (RfcommDevice) device;
        supervisor.connect(connected.getDevice());
    }

    @Override
    public List<Device> getDevices() {
        List<Device> devices = new ArrayList<>();

        for (BluetoothDevice device : devices()) {
            devices.add(new RfcommDevice(device));
        }

        return devices;
    }

    private Set<BluetoothDevice> devices() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        return (adapter == null) ? new HashSet<BluetoothDevice>() : adapter.getBondedDevices();
    }

    @Override
    public void send(List<Byte> data) {
        supervisor.send(data);
    }

    @Override
    public void poll() {
        supervisor.poll();
    }

    @Override
    public void received(List<Byte> list) {

    }

    @Override
    public void connected() {
        listener.connected(connected);
    }

    @Override
    public void disconnected() {
        connected = null;
        listener.disconnected();
    }

    @Override
    public void connecting(String s) {
    }

    @Override
    public void error(String s) {
    }
}
