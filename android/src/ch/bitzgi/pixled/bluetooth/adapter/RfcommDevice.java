/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.bluetooth.adapter;

import android.bluetooth.BluetoothDevice;

import ch.bitzgi.pixled.bluetooth.Device;

class RfcommDevice implements Device {
    private final BluetoothDevice device;

    public RfcommDevice(BluetoothDevice device) {
        this.device = device;
    }

    @Override
    public String getName() {
        return device.getName();
    }

    public BluetoothDevice getDevice() {
        return device;
    }
}
