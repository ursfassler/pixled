/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled;

import android.os.Bundle;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import ch.bitzgi.pixled.bluetooth.adapter.RfcommAdapter;
import ch.bitzgi.pixled.gdx.GdxMain;
import ch.bitzgi.pixled.gdx.NullPolling;

public class AndroidLauncher extends AndroidApplication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        RfcommAdapter adapter = new RfcommAdapter();

        RelativeLayout layout = new RelativeLayout(this);
        layout.addView(initializeForView(new GdxMain(adapter, new NullPolling(), adapter), config));
        setContentView(layout);
    }

}
