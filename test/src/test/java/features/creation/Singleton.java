/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.creation;

public class Singleton<T> {
    private final Class<T> clazz;
    private T instance;

    public Singleton(Class<T> clazz) {
        this.clazz = clazz;
    }

    public T get() {
        if (instance == null) {
            instance = tryCreate();
            if (instance == null) {
                throw new RuntimeException("could not create instance of type " + clazz.getName());
            }
        }

        return instance;
    }

    private T tryCreate() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void delete() {
        instance = null;
    }
}