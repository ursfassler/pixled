/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.image;

import ch.bitzgi.pixled.image.ImageLoader;
import cucumber.api.java.After;
import features.creation.Singleton;
import features.filesystem.FileContext;

public class ImageContext {
    static public final Singleton<ImageContext> Instance = new Singleton<ImageContext>(ImageContext.class);

    @After
    public void destroy_context() {
        ImageContext.Instance.delete();
    }

    public final ImageLoader imageLoader = new PpmImageLoader(FileContext.Instance.get().directory);
}
