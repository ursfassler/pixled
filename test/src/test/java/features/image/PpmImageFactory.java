/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.image;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.StringBufferInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import ch.bitzgi.pixled.image.Color;
import ch.bitzgi.pixled.image.Image;

public class PpmImageFactory {
    public static Image load(String content) {
        StringBufferInputStream stream = new StringBufferInputStream(content);
        BufferedImage rawimage = null;
        try {
            rawimage = ImageIO.read(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Image image = convert(rawimage);

        int maxValue = getMaximumValue(image.pixels);
        int factor = (maxValue == 0) ? 0 : 0xff / maxValue;

        image = new Image(image.width, image.height, scale(image.pixels, factor));
        return image;
    }

    private static List<Color> scale(List<Color> pixels, int factor) {
        List<Color> result = new ArrayList<Color>();
        for (Color pixel : pixels) {
            int scaled = pixel.value * factor;
            result.add(new Color(scaled));
        }
        return result;
    }

    private static int getMaximumValue(List<Color> pixels) {
        int result = 0;

        for (Color pixel : pixels) {
            result = Math.max(result, pixel.red());
            result = Math.max(result, pixel.green());
            result = Math.max(result, pixel.blue());
        }

        return result;
    }

    private static Image convert(BufferedImage rawimage) {
        int width = rawimage.getWidth();
        int height = rawimage.getHeight();

        List<Color> pixels = new ArrayList<Color>();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int rgb = rawimage.getRGB(x, y);
                Color color = convert(rgb);
                pixels.add(color);
            }
        }

        return new Image(width, height, pixels);
    }

    private static Color convert(int rgb) {
        return new Color(rgb & 0xffffff);
    }

    public static List<String> supportedExtensions() {
        List<String> result = new ArrayList<String>();
        result.add(".ppm");
        return result;
    }

}