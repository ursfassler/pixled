/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.image;

import java.util.List;

import ch.bitzgi.pixled.filesystem.File;
import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.image.ImageLoader;
import features.filesystem.InMemoryDirectory;
import features.filesystem.InMemoryFile;

class PpmImageLoader implements ImageLoader {
    private final InMemoryDirectory directory;

    public PpmImageLoader(InMemoryDirectory directory) {
        this.directory = directory;
    }

    @Override
    public Image load(File value) {
        InMemoryFile file = directory.get(value);
        String content = file.getContent();
        return PpmImageFactory.load(content);
    }

    @Override
    public List<String> supportedExtensions() {
        return PpmImageFactory.supportedExtensions();
    }

}
