/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features;

import ch.bitzgi.pixled.PixledFactory;
import ch.bitzgi.pixled.widget.PixletButton;
import ch.bitzgi.pixled.widget.SystemButton;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import features.bluetooth.BluetoothContext;
import features.filesystem.FileContext;
import features.image.ImageContext;
import features.widget.TestButton;
import features.widget.TestFileImage;
import features.widget.TestMemoryImage;
import features.widget.WidgetContext;

public class CoreSteps {

    @Given("^the app is started$")
    public void the_app_is_started() {
        i_start_the_app();
    }

    @When("^I start the app$")
    public void i_start_the_app() {
        CoreContext.Instance.pixled = PixledFactory.produce(BluetoothContext.Instance.get().adapter,
                FileContext.Instance.get().directory, ImageContext.Instance.get().imageLoader);
        CoreContext.Instance.pixled.boot();
        WidgetContext buttonContext = WidgetContext.Instance.get();
        for (SystemButton button : CoreContext.Instance.pixled.getSystem()) {
            String name = button.getName();
            buttonContext.widgets.add(name);
            buttonContext.fileImage.put(name, new TestFileImage(button.getImage()));
            buttonContext.buttons.put(name, new TestButton(button.getButton()));
        }
        for (PixletButton button : CoreContext.Instance.pixled.getContent()) {
            String name = button.getName();
            buttonContext.widgets.add(name);
            buttonContext.memoryImage.put(name, new TestMemoryImage(button.getImage()));
            buttonContext.buttons.put(name, new TestButton(button.getButton()));
        }
    }

}
