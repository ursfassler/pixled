/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features;

import ch.bitzgi.pixled.Pixled;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class CoreContext {
    static public CoreContext Instance = null;

    @Before
    public void instantiate_context() {
        CoreContext.Instance = new CoreContext();
    }

    @After
    public void destroy_context() {
        CoreContext.Instance = null;
    }

    public Pixled pixled;
}
