/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.widget;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class ButtonData {
    private final String name;
    private final String image;

    public ButtonData(String name, String image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "Button(" + name + ", " + image + ")";
    }

}