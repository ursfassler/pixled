/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.widget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cucumber.api.java.After;
import features.creation.Singleton;

public class WidgetContext {
    static public final Singleton<WidgetContext> Instance = new Singleton<WidgetContext>(WidgetContext.class);

    @After
    public void destroy_context() {
        WidgetContext.Instance.delete();
    }

    public final List<String> widgets = new ArrayList<String>();
    public final Map<String, TestButton> buttons = new HashMap<String, TestButton>();
    public final Map<String, TestFileImage> fileImage = new HashMap<String, TestFileImage>();
    public final Map<String, TestMemoryImage> memoryImage = new HashMap<String, TestMemoryImage>();
}