/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.widget;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import ch.bitzgi.pixled.image.Image;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.filesystem.FileContext;
import features.image.PpmImageFactory;

public class WidgetSteps {
    @When("^I click on the button \"([^\"]*)\"$")
    public void i_click_on_the_button(String name) {
        TestButton button = getButton(name);
        button.click();
    }

    @Then("^the following system widgets are available:$")
    public void the_following_system_widgets_are_available(List<ButtonData> expected) {
        List<ButtonData> actual = new ArrayList<ButtonData>();
        WidgetContext buttonContext = WidgetContext.Instance.get();
        for (String name : buttonContext.widgets) {
            TestFileImage image = buttonContext.fileImage.get(name);
            if (image != null) {
                actual.add(new ButtonData(name, image.getImageName()));
            }
        }
        assertEquals(expected, actual);
    }

    @Then("^the following content widgets are available:$")
    public void the_following_content_widgets_are_available(List<ButtonData> expected) {
        List<ButtonData> actual = new ArrayList<ButtonData>();
        WidgetContext buttonContext = WidgetContext.Instance.get();
        for (String name : buttonContext.widgets) {
            TestMemoryImage image = buttonContext.memoryImage.get(name);
            if (image != null) {
                actual.add(new ButtonData(name, ""));
            }
        }
        assertEquals(expected, actual);
    }

    @Then("^the button \"([^\"]*)\" has the image \"([^\"]*)\"$")
    public void the_button_has_the_image(String name, String expected) {
        String actual = getImageName(name);
        assertEquals(expected, actual);
    }

    @Then("^the button \"([^\"]*)\" is enabled$")
    public void the_button_is_enabled(String name) {
        TestButton button = getButton(name);
        assertTrue(button.getEnabled());
    }

    @Then("^the button \"([^\"]*)\" is disabled$")
    public void the_button_is_disabled(String name) {
        TestButton button = getButton(name);
        assertFalse(button.getEnabled());
    }

    @Then("^I expect the image of the button \"([^\"]*)\" to be:$")
    public void i_expect_the_image_of_the_button_to_be(String buttonName, String imageContent) {
        Image expected = PpmImageFactory.load(imageContent);
        Image actual = getImageData(buttonName);

        assertEquals(expected, actual);
    }

    @Then("^I expect the image of the button \"([^\"]*)\" to be the same as the file \"([^\"]*)\"$")
    public void i_expect_the_image_of_the_button_to_be_the_same_as_the_file(String buttonName, String imageFile) {
        Image expected = PpmImageFactory.load(FileContext.Instance.get().directory.readFile(imageFile));
        Image actual = getImageData(buttonName);

        assertEquals(expected, actual);
    }

    private String getImageName(String name) {
        WidgetContext buttonContext = WidgetContext.Instance.get();
        TestFileImage image = buttonContext.fileImage.get(name);
        return image.getImageName();
    }

    private Image getImageData(String name) {
        WidgetContext buttonContext = WidgetContext.Instance.get();
        TestMemoryImage image = buttonContext.memoryImage.get(name);
        return image.getImageData();
    }

    private TestButton getButton(String name) {
        WidgetContext buttonContext = WidgetContext.Instance.get();
        TestButton button = buttonContext.buttons.get(name);
        return button;
    }

}
