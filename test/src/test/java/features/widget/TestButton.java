/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.widget;

import ch.bitzgi.pixled.widget.Button;
import ch.bitzgi.pixled.widget.ButtonStateListener;

public class TestButton implements ButtonStateListener {
    private final Button button;
    private Boolean enabled;

    public TestButton(Button button) {
        this.button = button;
        this.button.setListener(this);
        enabledChanged();
    }

    public void click() {
        button.click();
    }

    @Override
    public void enabledChanged() {
        enabled = button.getEnabled();
    }

    public Boolean getEnabled() {
        return enabled;
    }

}