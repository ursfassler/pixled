/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.widget;

import ch.bitzgi.pixled.widget.FileImage;
import ch.bitzgi.pixled.widget.ImageListener;

public class TestFileImage implements ImageListener {
    private final FileImage image;
    private String imageName = "";

    public TestFileImage(FileImage image) {
        this.image = image;
        this.image.setListener(this);
        imageChanged();
    }

    @Override
    public void imageChanged() {
        this.imageName = image.getImage();
    }

    public String getImageName() {
        return imageName;
    }

}
