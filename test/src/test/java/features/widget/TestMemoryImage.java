/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.widget;

import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.widget.ImageListener;
import ch.bitzgi.pixled.widget.MemoryImage;

public class TestMemoryImage implements ImageListener {
    private final MemoryImage image;
    private Image imageData;

    public TestMemoryImage(MemoryImage image) {
        this.image = image;
        this.image.setListener(this);
        imageChanged();
    }

    @Override
    public void imageChanged() {
        imageData = image.getImage();
    }

    public Image getImageData() {
        return imageData;
    }

}
