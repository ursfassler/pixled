/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.filesystem;

import cucumber.api.java.After;
import features.creation.Singleton;

public class FileContext {
    static public final Singleton<FileContext> Instance = new Singleton<FileContext>(FileContext.class);

    @After
    public void destroy_context() {
        FileContext.Instance.delete();
    }

    public final InMemoryDirectory directory = new InMemoryDirectory();
}
