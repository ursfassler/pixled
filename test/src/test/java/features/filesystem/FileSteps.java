/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.filesystem;

import cucumber.api.java.en.Given;

public class FileSteps {
    @Given("^I have the file \"([^\"]*)\":$")
    public void i_have_the_image(String path, String content) {
        FileContext.Instance.get().directory.writeFile(path, content);
    }

}
