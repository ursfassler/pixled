/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.filesystem;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import ch.bitzgi.pixled.filesystem.Directory;
import ch.bitzgi.pixled.filesystem.File;
import ch.bitzgi.pixled.filesystem.FileSystem;

public class InMemoryDirectory implements Directory {
    private final Set<InMemoryFile> files = new HashSet<InMemoryFile>();
    private final Set<InMemoryDirectory> directories = new HashSet<InMemoryDirectory>();
    private final Directory parent;
    private final String name;

    public InMemoryDirectory(Directory parent, String name) {
        this.parent = parent;
        this.name = name;
    }

    public InMemoryDirectory() {
        this.parent = null;
        this.name = "";
    }

    @Override
    public Set<? extends File> listFiles() {
        return Collections.unmodifiableSet(files);
    }

    public void writeFile(String path, String content) {
        String[] structure = path.split("/");

        InMemoryDirectory directory = this;
        for (int i = 0; i < structure.length - 1; i++) {
            directory = directory.create(structure[i]);
        }

        directory.files.add(new InMemoryFile(this, structure[structure.length - 1], content));
    }

    public String readFile(String path) {
        String[] structure = path.split("/");

        Directory directory = this;
        for (int i = 0; i < structure.length - 1; i++) {
            directory = FileSystem.findDirectory(directory, structure[i]);
        }

        File file = FileSystem.findFile(directory, structure[structure.length - 1]);
        return ((InMemoryFile) file).getContent();
    }

    private InMemoryDirectory create(String name) {
        for (InMemoryDirectory directory : directories) {
            if (directory.name.equals(name)) {
                return directory;
            }
        }

        InMemoryDirectory directory = new InMemoryDirectory(this, name);
        directories.add(directory);
        return directory;
    }

    public InMemoryFile get(File value) {
        if (value instanceof InMemoryFile) {
            return (InMemoryFile) value;
        } else {
            throw new IllegalArgumentException("file not found: " + value);
        }
    }

    @Override
    public Set<? extends Directory> listDirectories() {
        return directories;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Directory parent() {
        return parent;
    }

}