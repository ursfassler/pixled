/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.filesystem;

import ch.bitzgi.pixled.filesystem.Directory;
import ch.bitzgi.pixled.filesystem.File;

public class InMemoryFile implements File {
    private final Directory parent;
    private final String name;
    private final String content;

    InMemoryFile(Directory parent, String name, String content) {
        this.parent = parent;
        this.name = name;
        this.content = content;
    }

    @Override
    public String name() {
        return name;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public Directory parent() {
        return parent;
    }

}