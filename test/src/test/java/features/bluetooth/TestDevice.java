/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.bluetooth;

import ch.bitzgi.pixled.bluetooth.Device;

public class TestDevice implements Device {
    private final String name;

    public TestDevice(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

}
