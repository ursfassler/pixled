/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.bluetooth;

import java.util.ArrayList;
import java.util.List;

import ch.bitzgi.pixled.bluetooth.Adapter;
import ch.bitzgi.pixled.bluetooth.ConnectionListener;
import ch.bitzgi.pixled.bluetooth.Device;

public class TestAdapter implements Adapter {
    private final List<Device> devices = new ArrayList<Device>();
    private final List<Byte> sent = new ArrayList<Byte>();
    private Device connectedDevice;
    private ConnectionListener listener;

    @Override
    public void connectTo(Device device, ConnectionListener listener) {
        connectedDevice = device;
        this.listener = listener;
    }

    @Override
    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<? extends Device> devices) {
        this.devices.clear();
        this.devices.addAll(devices);
    }

    @Override
    public void send(List<Byte> data) {
        sent.addAll(data);
    }

    public String sentAsString() {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (byte sym : sent) {
            if (first) {
                first = false;
            } else {
                sb.append(" ");
            }
            sb.append(String.format("%02X", sym));
        }
        return sb.toString();
    }

    public void setConnected() {
        assert connectedDevice != null;
        assert listener != null;
        listener.connected(connectedDevice);
    }

    public void setDisconnected() {
        assert connectedDevice != null;
        assert listener != null;
        ConnectionListener notify = listener;
        connectedDevice = null;
        listener = null;
        notify.disconnected();
    }

    public Device getConnectedDevice() {
        return connectedDevice;
    }

    public void clear() {
        sent.clear();
    }

}
