/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.bluetooth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import ch.bitzgi.pixled.bluetooth.Device;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BluetoothSteps {

    @Given("^I have the following devices:$")
    public void i_have_the_following_devices(List<TestDevice> devices) {
        BluetoothContext.Instance.get().adapter.setDevices(devices);
    }

    @Given("^I clear the bytes sent to the device$")
    public void i_clear_the_bytes_sent_to_the_device() {
        BluetoothContext.Instance.get().adapter.clear();
    }

    @When("^the connection is established$")
    public void the_connection_is_established() {
        BluetoothContext.Instance.get().adapter.setConnected();
    }

    @When("^the device disconnects$")
    public void the_device_disconnects() {
        BluetoothContext.Instance.get().adapter.setDisconnected();
    }

    @Then("^the following bytes are sent to the device: \"([^\"]*)\"$")
    public void the_following_bytes_are_sent_to_the_device(String expected) {
        String actual = BluetoothContext.Instance.get().adapter.sentAsString();
        assertEquals(expected, actual);
    }

    @Then("^no connection is started$")
    public void no_connection_is_started() {
        Device connectingDevice = BluetoothContext.Instance.get().adapter.getConnectedDevice();
        assertNull(connectingDevice);
    }

    @Then("^a connection to \"([^\"]*)\" is started$")
    public void a_connection_to_is_started(String expected) {
        String actual = BluetoothContext.Instance.get().adapter.getConnectedDevice().getName();
        assertEquals(expected, actual);
    }

    @Then("^the app is connected to \"([^\"]*)\"$")
    public void the_app_is_connected_to(String expected) {
        String actual = BluetoothContext.Instance.get().adapter.getConnectedDevice().getName();
        assertEquals(expected, actual);
    }

}
