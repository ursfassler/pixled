/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package features.bluetooth;

import cucumber.api.java.After;
import features.creation.Singleton;

public class BluetoothContext {
    static public final Singleton<BluetoothContext> Instance = new Singleton<BluetoothContext>(BluetoothContext.class);

    @After
    public void destroy_context() {
        BluetoothContext.Instance.delete();
    }

    public TestAdapter adapter = new TestAdapter();
}