# Copyright 2019 Urs Fässler
# SPDX-License-Identifier: GPL-3.0-only

Feature: switch screens

As a user
I want to switch the screen of the AuraBox
In order to see the screen I want


Background:
  Given I have the file "screen/Temperature-Celsius.ppm":
    """
	P3
	1 1
	1
	0 0 1
    """
  And I have the file "screen/Temperature-Farenheit.ppm":
    """
	P3
	1 1
	1
	0 0 1
    """
  And I have the following devices:
    | name    |
    | AuraBox |
  And the app is started
  And the connection is established


Scenario: switch to the time screen
  When I click on the button "switchToTime"

  Then the following bytes are sent to the device: "01 04 00 45 00 49 00 02"


Scenario: switch to the temperature screen
  When I click on the button "switchToTemperature"

  Then the following bytes are sent to the device: "01 04 00 45 03 04 4A 00 02"
  And I expect the image of the button "switchToTemperature" to be the same as the file "screen/Temperature-Celsius.ppm"


Scenario: switch the temperature unit to Farenheit
  Given I click on the button "switchToTemperature"
  And I clear the bytes sent to the device

  When I click on the button "switchToTemperature"

  Then the following bytes are sent to the device: "01 04 00 4C 03 04 51 00 02"
  And I expect the image of the button "switchToTemperature" to be the same as the file "screen/Temperature-Farenheit.ppm"


Scenario: switch the temperature unit to Celsius
  Given I click on the button "switchToTemperature"
  And I click on the button "switchToTemperature"
  And I clear the bytes sent to the device

  When I click on the button "switchToTemperature"

  Then the following bytes are sent to the device: "01 04 00 4C 00 50 00 02"
  And I expect the image of the button "switchToTemperature" to be the same as the file "screen/Temperature-Celsius.ppm"


Scenario: switch to the light screen
  When I click on the button "switchToLight"

  Then the following bytes are sent to the device: "01 04 00 45 03 05 4B 00 02"
  And I expect the image of the button "switchToLight" to be:
    """
	P3
	10 10
	1
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
    """


Scenario: switch the light screen to green
  Given I click on the button "switchToLight"
  And I clear the bytes sent to the device

  When I click on the button "switchToLight"

  Then the following bytes are sent to the device: "01 04 00 47 03 05 4D 00 02"
  And I expect the image of the button "switchToLight" to be:
    """
	P3
	10 10
	1
	0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0
	0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0
	0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0
	0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0
	0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0
	0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0
	0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0
	0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0
	0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0
	0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0   0 1 0
    """


Scenario: switch the light screen to white
  Given I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I clear the bytes sent to the device

  When I click on the button "switchToLight"

  Then the following bytes are sent to the device: "01 04 00 47 07 52 00 02"


Scenario: switch the light screen to cyan
  Given I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I clear the bytes sent to the device

  When I click on the button "switchToLight"

  Then the following bytes are sent to the device: "01 04 00 47 06 51 00 02"


Scenario: switch the light screen to blue
  Given I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I clear the bytes sent to the device

  When I click on the button "switchToLight"

  Then the following bytes are sent to the device: "01 04 00 47 04 4F 00 02"


Scenario: switch the light screen to purple
  Given I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I clear the bytes sent to the device

  When I click on the button "switchToLight"

  Then the following bytes are sent to the device: "01 04 00 47 05 50 00 02"


Scenario: switch the light screen to red
  Given I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I clear the bytes sent to the device

  When I click on the button "switchToLight"

  Then the following bytes are sent to the device: "01 04 00 47 03 04 4C 00 02"


Scenario: switch the light screen to yellow
  Given I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I click on the button "switchToLight"
  And I clear the bytes sent to the device

  When I click on the button "switchToLight"

  Then the following bytes are sent to the device: "01 04 00 47 03 06 4E 00 02"


Scenario: switch to the equalizer screen
  When I click on the button "switchToEqualizer"

  Then the following bytes are sent to the device: "01 04 00 45 03 06 4C 00 02"


Scenario: switch to the imagination screen
  When I click on the button "switchToImagination"

  Then the following bytes are sent to the device: "01 04 00 45 04 4D 00 02"


Scenario: switch to the party screen
  When I click on the button "switchToParty"

  Then the following bytes are sent to the device: "01 04 00 45 05 4E 00 02"


Scenario: switch to the rainbow screen
  When I click on the button "switchToRainbow"

  Then the following bytes are sent to the device: "01 04 00 45 06 4F 00 02"


Scenario: switch to the explosion screen
  When I click on the button "switchToExplosion"

  Then the following bytes are sent to the device: "01 04 00 45 07 50 00 02"
