# Copyright 2019 Urs Fässler
# SPDX-License-Identifier: GPL-3.0-only

Feature: widgets and layout of the main page

As a user
I want to see the important information on the main page
In order get an overview at a glance


Scenario: the widgets are available
  Given I have the file "images/01 image 2.ppm":
    """
	P3
	10 10
	1
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
    """
  And I have the file "images/00 image 1.ppm":
    """
	P3
	10 10
	1
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
    """
  And I have the file "images/02 image 3.ppm":
    """
	P3
	1 11
	1
	0 0 0
	0 0 0
	0 0 0
	0 0 0
	0 0 0
	0 0 0
	0 0 0
	0 0 0
	0 0 0
	0 0 0
	0 0 0
    """
  And I have the file "screen/Time.ppm":
    """
	P3
	2 2
	1
	1 0 0   0 1 0
	0 0 1   0 0 0
    """
  And I have the file "screen/Temperature-Celsius.ppm":
    """
	P3
	1 1
	1
	0 0 1
    """
  And I have the file "screen/Temperature-Farenheit.ppm":
    """
	P3
	1 1
	1
	0 1 0
    """
  And I have the file "screen/Equalizer.ppm":
    """
	P3
	1 1
	1
	0 1 1
    """
  And I have the file "screen/Imagination.ppm":
    """
	P3
	1 1
	1
	1 0 0
    """
  And I have the file "screen/Party.ppm":
    """
	P3
	1 1
	1
	1 0 1
    """
  And I have the file "screen/Rainbow.ppm":
    """
	P3
	1 1
	1
	1 1 0
    """
  And I have the file "screen/Explosion.ppm":
    """
	P3
	1 1
	1
	1 1 1
    """

  When I start the app
  
  Then the following system widgets are available:
    | name                | image             |
    | connection          | disconnected      |
    | brightness          | brightnessLight   |
  And the following content widgets are available:
    | name                | image             |
    | switchToTime        |                   |
    | switchToTemperature |                   |
    | switchToLight       |                   |
    | switchToEqualizer   |                   |
    | switchToImagination |                   |
    | switchToParty       |                   |
    | switchToRainbow     |                   |
    | switchToExplosion   |                   |
    | image 00 image 1    |                   |
    | image 01 image 2    |                   |
  And I expect the image of the button "image 00 image 1" to be:
    """
	P3
	10 10
	1
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
	0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0   0 0 0
    """
  And I expect the image of the button "switchToTime" to be:
    """
	P3
	2 2
	1
	1 0 0   0 1 0
	0 0 1   0 0 0
    """
  And I expect the image of the button "switchToTemperature" to be the same as the file "screen/Temperature-Celsius.ppm"
  And I expect the image of the button "switchToLight" to be:
    """
	P3
	10 10
	1
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
	1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0   1 1 0
    """
  And I expect the image of the button "switchToEqualizer" to be the same as the file "screen/Equalizer.ppm"
  And I expect the image of the button "switchToImagination" to be the same as the file "screen/Imagination.ppm"
  And I expect the image of the button "switchToParty" to be the same as the file "screen/Party.ppm"
  And I expect the image of the button "switchToRainbow" to be the same as the file "screen/Rainbow.ppm"
  And I expect the image of the button "switchToExplosion" to be the same as the file "screen/Explosion.ppm"
