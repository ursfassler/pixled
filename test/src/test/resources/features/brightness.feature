# Copyright 2019 Urs Fässler
# SPDX-License-Identifier: GPL-3.0-only

Feature: change the brightness

As a user
I want to change the screen brightness
In order to adjust it to the surrounding


Background:
  Given I have the following devices:
    | name    |
    | AuraBox |
  And the app is started
  And the connection is established


Scenario: set brightness to dark
  When I click on the button "brightness"

  Then the following bytes are sent to the device: "01 04 00 32 3F 75 00 02"
  And the button "brightness" has the image "brightnessDark"


Scenario: set brightness to off
  Given I click on the button "brightness"
  And I clear the bytes sent to the device

  When I click on the button "brightness"

  Then the following bytes are sent to the device: "01 04 00 32 00 36 00 02"
  And the button "brightness" has the image "brightnessOff"


Scenario: set brightness to light
  Given I click on the button "brightness"
  And I click on the button "brightness"
  And I clear the bytes sent to the device

  When I click on the button "brightness"

  Then the following bytes are sent to the device: "01 04 00 32 D2 08 03 04 02"
  And the button "brightness" has the image "brightnessLight"


Scenario: reset brightness when connected
  Given I click on the button "brightness"
  And the device disconnects
  And I click on the button "connection"
  And I clear the bytes sent to the device

  When the connection is established

  Then the button "brightness" has the image "brightnessLight"
  And the following bytes are sent to the device: "01 04 00 32 D2 08 03 04 02"


Scenario: brightness is enabled when connected

  Then the button "brightness" is enabled


Scenario: brightness is disabled when disconnected

  When the device disconnects

  Then the button "brightness" is disabled
