# Copyright 2019 Urs Fässler
# SPDX-License-Identifier: GPL-3.0-only

Feature: connect to device

As a user
I want connect the app to my device
In order to control the screen of the device


Scenario: automatically connect when the device is available while booting
  Given I have the following devices:
    | name           |
    | Laptop         |
    | AuraBox-light  |
    | another device |

  When I start the app

  Then a connection to "AuraBox-light" is started
  And the button "connection" has the image "connecting"


Scenario: display the device name we are connected to
  Given I have the following devices:
    | name           |
    | Laptop         |
    | AuraBox-light  |
    | another device |
  And the app is started

  When the connection is established

  Then the app is connected to "AuraBox-light"
  And the button "connection" has the image "connected"


Scenario: update the connection icon when disconnected
  Given I have the following devices:
    | name           |
    | Laptop         |
    | AuraBox-light  |
    | another device |
  And the app is started
  And the connection is established

  When the device disconnects

  Then the button "connection" has the image "disconnected"


Scenario: don't send data when disconnected
  Given I have the following devices:
    | name           |
    | Laptop         |
    | AuraBox-light  |
    | another device |
  And the app is started
  And the device disconnects

  When I click on the button "switchToTime"

  Then the following bytes are sent to the device: ""


Scenario: has no connection when no device is available
  Given I have the following devices:
    | name           |
    | Laptop         |
    | another device |

  When I start the app

  Then no connection is started
  And the button "connection" has the image "disconnected"


Scenario: connect manually to the device
  Given the app is started

  When I have the following devices:
    | name           |
    | AuraBox-light  |
  And I click on the button "connection"

  Then the button "connection" has the image "connecting"
  And a connection to "AuraBox-light" is started
