/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.divoom.aurabox;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ch.bitzgi.pixled.image.Color;

public class ColorConverter_Test {
    @Test
    public void convert_black() {
        assertEquals(0, ColorConverter.to3Bit(Color.BLACK));
    }

    @Test
    public void convert_white() {
        assertEquals(7, ColorConverter.to3Bit(Color.WHITE));
    }

    @Test
    public void convert_red() {
        assertEquals(1, ColorConverter.to3Bit(Color.RED));
    }

    @Test
    public void convert_green() {
        assertEquals(2, ColorConverter.to3Bit(Color.GREEN));
    }

    @Test
    public void convert_blue() {
        assertEquals(4, ColorConverter.to3Bit(Color.BLUE));
    }
}
