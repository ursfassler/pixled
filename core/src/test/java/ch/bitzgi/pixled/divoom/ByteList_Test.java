package ch.bitzgi.pixled.divoom;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ByteList_Test {
    @Test
    public void convert_0_to_byte() {
        assertEquals(0, ByteList.asByte(0));
    }

    @Test
    public void convert_0_to_int() {
        assertEquals(0, ByteList.asInt((byte) 0));
    }

    @Test
    public void convert_1_to_byte() {
        assertEquals(1, ByteList.asByte(1));
    }

    @Test
    public void convert_1_to_int() {
        assertEquals(1, ByteList.asInt((byte) 1));
    }

    @Test
    public void convert_0x7f_to_byte() {
        assertEquals(0x7f, ByteList.asByte(0x7f));
    }

    @Test
    public void convert_0x7f_to_int() {
        assertEquals(0x7f, ByteList.asInt((byte) 0x7f));
    }

    @Test
    public void convert_0x80_to_byte() {
        assertEquals(-128, ByteList.asByte(0x80));
    }

    @Test
    public void convert_0x80_to_int() {
        assertEquals(0x80, ByteList.asInt((byte) 0x80));
    }

    @Test
    public void convert_0xff_to_byte() {
        assertEquals(-1, ByteList.asByte(0xff));
    }

    @Test
    public void convert_0xff_to_int() {
        assertEquals(0xff, ByteList.asInt((byte) 0xff));
    }

}
