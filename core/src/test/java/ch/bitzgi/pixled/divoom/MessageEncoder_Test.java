package ch.bitzgi.pixled.divoom;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MessageEncoder_Test {
    private final MessageEncoder testee = new MessageEncoder();
    private final List<Byte> message = new ArrayList<Byte>();

    @Test
    public void encode_large_checksum_correctly() {
        message.add((byte) 0xf0);
        message.add((byte) 0xf0);

        List<Byte> encoded = testee.encode(message);

        assertEquals(6, encoded.size());
        assertEquals(0xe4, ByteList.asInt(encoded.get(4)));
        assertEquals(0x01, ByteList.asInt(encoded.get(5)));
    }

}
