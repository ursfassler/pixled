/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.image;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import ch.bitzgi.pixled.image.Color;

public class ImageColors_Test {
    @Test
    public void returns_false_when_image_is_all_black() {
        List<Color> pixels = list(Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK);

        boolean result = ImageColors.hasOneColor(pixels);

        assertFalse(result);
    }

    @Test
    public void returns_true_when_one_pixel_is_yellow() {
        List<Color> pixels = list(Color.BLACK, Color.YELLOW, Color.BLACK, Color.BLACK);

        boolean result = ImageColors.hasOneColor(pixels);

        assertTrue(result);
    }

    @Test
    public void returns_true_when_multiple_pixels_are_green() {
        List<Color> pixels = list(Color.GREEN, Color.BLACK, Color.GREEN, Color.BLACK);

        boolean result = ImageColors.hasOneColor(pixels);

        assertTrue(result);
    }

    @Test
    public void returns_false_when_pixels_have_different_colors() {
        List<Color> pixels = list(Color.GREEN, Color.BLACK, Color.YELLOW, Color.BLACK);

        boolean result = ImageColors.hasOneColor(pixels);

        assertFalse(result);
    }

    @Test
    public void returns_false_when_only_two_colors_are_used_but_not_black() {
        List<Color> pixels = list(Color.GREEN, Color.GREEN, Color.YELLOW, Color.YELLOW);

        boolean result = ImageColors.hasOneColor(pixels);

        assertFalse(result);
    }

    @Test
    public void return_first_color_that_is_not_black() {
        List<Color> pixels = list(Color.BLACK, Color.GREEN, Color.RED, Color.YELLOW);

        Color color = ImageColors.getSingleColor(pixels);

        assertEquals(Color.GREEN, color);
    }

    @Test
    public void return_black_when_no_pixel_is_colored() {
        List<Color> pixels = list(Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK);

        Color color = ImageColors.getSingleColor(pixels);

        assertEquals(Color.BLACK, color);
    }

    private List<Color> list(Color c1, Color c2, Color c3, Color c4) {
        List<Color> result = new ArrayList<Color>();

        result.add(c1);
        result.add(c2);
        result.add(c3);
        result.add(c4);

        return result;
    }

}
