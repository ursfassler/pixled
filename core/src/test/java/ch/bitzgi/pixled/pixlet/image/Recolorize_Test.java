/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.image;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import ch.bitzgi.pixled.image.Color;

public class Recolorize_Test {
    @Test
    public void does_nothing_when_no_colorized_pixels_are_provided() {
        List<Color> pixels = list(Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK);
        List<Color> expected = list(Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK);

        List<Color> actual = Recolorize.colorize(pixels, Color.YELLOW);

        assertEquals(expected, actual);
    }

    @Test
    public void replace_colored_pixels_with_new_color() {
        List<Color> pixels = list(Color.BLACK, Color.GREEN, Color.BLACK, Color.BLACK);
        List<Color> expected = list(Color.BLACK, Color.PURPLE, Color.BLACK, Color.BLACK);

        List<Color> actual = Recolorize.colorize(pixels, Color.PURPLE);

        assertEquals(expected, actual);
    }

    private List<Color> list(Color c1, Color c2, Color c3, Color c4) {
        List<Color> result = new ArrayList<Color>();

        result.add(c1);
        result.add(c2);
        result.add(c3);
        result.add(c4);

        return result;
    }

}
