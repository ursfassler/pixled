/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet;

import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.Test;

public class PixletActivator_Test {
    private final PixletActivator testee = new PixletActivator();

    @Test
    public void activates_pixlet_when_first_clicked() {
        Pixlet pixlet = mock(Pixlet.class);

        testee.clicked(pixlet);

        verify(pixlet).activate();
        verify(pixlet, never()).change();
    }

    @Test
    public void send_change_when_clicked_a_second_time() {
        Pixlet pixlet = mock(Pixlet.class);
        testee.clicked(pixlet);
        clearInvocations(pixlet);

        testee.clicked(pixlet);

        verify(pixlet).change();
        verify(pixlet, never()).activate();
    }

}
