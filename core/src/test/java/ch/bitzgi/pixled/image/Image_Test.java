/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.image;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class Image_Test {
    private final Image image1 = new Image(2, 2, createPixels(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE));
    private final Image image2 = new Image(2, 2, createPixels(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE));
    private final Image image3 = new Image(2, 2, createPixels(Color.BLACK, Color.GREEN, Color.BLUE, Color.WHITE));

    @Test
    public void can_create_string_representation() {

        String actual = image1.toString();

        assertEquals("2*2 #FF0000 #00FF00 #0000FF #FFFFFF", actual);
    }

    @Test
    public void are_equal_when_content_is_equal() {
        assertEquals(image1, image2);
    }

    @Test
    public void are_not_equal_when_content_is_not_equal() {
        assertNotEquals(image1, image3);
    }

    private List<Color> createPixels(Color a1, Color a2, Color a3, Color a4) {
        List<Color> pixels = new ArrayList<Color>();
        pixels.add(a1);
        pixels.add(a2);
        pixels.add(a3);
        pixels.add(a4);
        return pixels;
    }

}
