/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.image;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class NearestColor_Test {
    private final static Set<Color> palette = createPalette();

    @Test
    public void return_same_color_when_in_palette() {

        Color actual = NearestColor.nearest(Color.BLUE, palette);

        assertEquals(Color.BLUE, actual);
    }

    @Test
    public void return_nearest_color_when_not_in_palette() {

        Color actual = NearestColor.nearest(Color.RED, palette);

        assertEquals(Color.YELLOW, actual);
    }

    @Test
    public void distance_is_0_when_color_are_the_same() {
        assertEquals(0, NearestColor.distance(Color.PURPLE, Color.PURPLE));
    }

    @Test
    public void distance_is_ff_when_color_are_black_and_blue() {
        assertEquals(0xff, NearestColor.distance(Color.BLACK, Color.BLUE));
    }

    @Test
    public void distance_is_ff_when_color_are_black_and_green() {
        assertEquals(0xff, NearestColor.distance(Color.BLACK, Color.GREEN));
    }

    @Test
    public void distance_is_ff_when_color_are_black_and_red() {
        assertEquals(0xff, NearestColor.distance(Color.BLACK, Color.RED));
    }

    @Test
    public void distance_is_maximal_when_color_are_black_and_white() {
        assertEquals(3 * 0xff, NearestColor.distance(Color.BLACK, Color.WHITE));
    }

    private static Set<Color> createPalette() {
        Set<Color> result = new HashSet<Color>();

        result.add(Color.BLUE);
        result.add(Color.CYAN);
        result.add(Color.YELLOW);

        return result;
    }

}
