/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.image;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Color_Test {

    @Test
    public void alpha_channel_is_removed_when_created() {
        Color color = new Color(0xaa112233);

        assertEquals(0x112233, color.value);
    }

}
