/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.image;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ColorSeries_Test {

    @Test
    public void starts_at_the_color_provided_when_initializing() {

        ColorSeries testee = new ColorSeries(Color.BLUE);

        assertEquals(Color.BLUE, testee.current());
    }

    @Test
    public void starts_at_the_nearest_color_to_the_one_provided_when_initializing() {

        ColorSeries testee = new ColorSeries(new Color(0xf01020));

        assertEquals(Color.RED, testee.current());
    }

    @Test
    public void can_loop_through_color_series() {
        ColorSeries testee = new ColorSeries(Color.BLACK);
        Color start = testee.current();

        for (int i = 0; i < 1000; i++) {
            testee.next();
            if (testee.current().equals(start)) {
                return;
            }
        }

        assertTrue(false);
    }

}
