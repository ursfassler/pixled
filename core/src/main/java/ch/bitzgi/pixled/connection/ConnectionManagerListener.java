/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.connection;

public interface ConnectionManagerListener {

    void connectionStateChanged(ConnectionState state);

}