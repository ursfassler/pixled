/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.connection;

import java.util.ArrayList;
import java.util.List;

import ch.bitzgi.pixled.bluetooth.Adapter;
import ch.bitzgi.pixled.bluetooth.ConnectionListener;
import ch.bitzgi.pixled.bluetooth.Device;

public class ConnectionManager implements ConnectionListener {
    private final Adapter adapter;
    private final List<ConnectionManagerListener> listeners = new ArrayList<ConnectionManagerListener>();
    private ConnectionState state = ConnectionState.Disconnected;
    private Device connectedDevice;

    public ConnectionManager(Adapter adapter) {
        this.adapter = adapter;
    }

    public void connect() {
        if (state == ConnectionState.Disconnected) {
            for (Device device : adapter.getDevices()) {
                if (canConnect(device)) {
                    setState(ConnectionState.Connecting);
                    adapter.connectTo(device, this);
                    return;
                }
            }
        }
    }

    private boolean canConnect(Device device) {
        return device.getName().contains("AuraBox");
    }

    @Override
    public void connected(Device device) {
        connectedDevice = device;
        setState(ConnectionState.Connected);
    }

    @Override
    public void disconnected() {
        connectedDevice = null;
        setState(ConnectionState.Disconnected);
    }

    private void setState(ConnectionState value) {
        if (state != value) {
            state = value;
            for (ConnectionManagerListener listener : listeners) {
                listener.connectionStateChanged(state);
            }
        }
    }

    public ConnectionState getState() {
        return state;
    }

    @Override
    public void received(List<Byte> data) {
    }

    public Device getConnectedDevice() {
        return connectedDevice;
    }

    public void addListener(ConnectionManagerListener value) {
        listeners.add(value);
    }

}