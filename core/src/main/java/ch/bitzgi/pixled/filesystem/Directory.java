/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.filesystem;

import java.util.Set;

public interface Directory extends File {

    Set<? extends File> listFiles();

    Set<? extends Directory> listDirectories();

}