/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.filesystem;

import java.util.ArrayList;
import java.util.List;

public class FileSystem {
    public static String path(File file) {
        List<String> list = pathList(file);
        String result = "";
        boolean first = true;
        for (String part : list) {
            if (first) {
                first = false;
            } else {
                result += "/";
            }
            result += part;
        }
        return result;
    }

    private static List<String> pathList(File file) {
        if (file.name().equals("")) {
            return new ArrayList<String>();
        } else {
            List<String> result = pathList(file.parent());
            result.add(file.name());
            return result;
        }
    }

    public static Directory findDirectory(Directory parent, String child) {
        for (Directory directory : parent.listDirectories()) {
            if (directory.name().equals(child)) {
                return directory;
            }
        }
        return null;
    }

    public static File findFile(Directory parent, String child) {
        for (File file : parent.listFiles()) {
            if (file.name().equals(child)) {
                return file;
            }
        }
        return null;
    }

    public static File findFile(Directory directory, String name, List<String> extensions) {
        for (String extension : extensions) {
            File file = findFile(directory, name + extension);
            if (file != null) {
                return file;
            }
        }
        return null;
    }

}