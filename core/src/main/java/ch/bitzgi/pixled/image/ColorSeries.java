/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.image;

import java.util.HashMap;
import java.util.Map;

public class ColorSeries {
    private final static Map<Color, Color> colors = createSeries();
    private Color currentColor;

    public ColorSeries(Color start) {
        assert start != null;

        this.currentColor = NearestColor.nearest(start, colors.keySet());
    }

    private static Map<Color, Color> createSeries() {
        Map<Color, Color> colors = new HashMap<Color, Color>();

        colors.put(Color.YELLOW, Color.GREEN);
        colors.put(Color.GREEN, Color.WHITE);
        colors.put(Color.WHITE, Color.CYAN);
        colors.put(Color.CYAN, Color.BLUE);
        colors.put(Color.BLUE, Color.PURPLE);
        colors.put(Color.PURPLE, Color.RED);
        colors.put(Color.RED, Color.YELLOW);

        return colors;
    }

    public void next() {
        currentColor = colors.get(currentColor);
        assert currentColor != null;
    }

    public Color current() {
        return currentColor;
    }

}