/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.image;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Color {
    final public static Color BLACK = new Color(0x000000);
    final public static Color RED = new Color(0xff0000);
    final public static Color GREEN = new Color(0x00ff00);
    final public static Color YELLOW = new Color(0xffff00);
    final public static Color BLUE = new Color(0x0000ff);
    final public static Color PURPLE = new Color(0xff00ff);
    final public static Color CYAN = new Color(0x00ffff);
    final public static Color WHITE = new Color(0xffffff);

    final public int value;

    public Color(int value) {
        this.value = value & 0xffffff;
    }

    @Override
    public String toString() {
        return String.format("#%06X", value);
    }

    public int red() {
        return (value >> 16) & 0xff;
    }

    public int green() {
        return (value >> 8) & 0xff;
    }

    public int blue() {
        return (value >> 0) & 0xff;
    }

}