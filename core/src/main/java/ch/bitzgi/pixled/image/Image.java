/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.image;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Image {
    public final int width;
    public final int height;
    public final List<Color> pixels;

    public Image(int width, int height, List<Color> pixels) {
        assert width >= 0;
        assert height >= 0;
        assert pixels.size() == (width * height);

        this.width = width;
        this.height = height;
        this.pixels = Collections.unmodifiableList(pixels);
    }

    public Image() {
        this.width = 0;
        this.height = 0;
        this.pixels = Collections.unmodifiableList(new ArrayList<Color>());
    }

    @Override
    public String toString() {
        String result = width + "*" + height;
        for (Color pixel : pixels) {
            result += " " + pixel.toString();
        }
        return result;
    }

    public Color getPixel(int x, int y) {
        assert x >= 0;
        assert x < width;
        assert y >= 0;
        assert y < height;

        int index = x + (y * height);
        return pixels.get(index);
    }

}
