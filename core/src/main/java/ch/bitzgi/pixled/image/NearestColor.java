/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.image;

import java.util.Set;

public class NearestColor {

    public static Color nearest(Color value, Set<Color> palette) {
        assert value != null;
        assert !palette.isEmpty();

        Color nearest = null;
        int nearestDistance = Integer.MAX_VALUE;
        for (Color color : palette) {
            int currentDistance = distance(color, value);
            if (currentDistance < nearestDistance) {
                nearestDistance = currentDistance;
                nearest = color;
            }
        }
        return nearest;
    }

    public static int distance(Color left, Color right) {
        return distance(left.red(), right.red()) + distance(left.green(), right.green())
                + distance(left.blue(), right.blue());
    }

    private static int distance(int left, int right) {
        return Math.abs(left - right);
    }

}