/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.image;

import java.util.List;

import ch.bitzgi.pixled.filesystem.File;

public interface ImageLoader {
    Image load(File file);

    List<String> supportedExtensions();
}