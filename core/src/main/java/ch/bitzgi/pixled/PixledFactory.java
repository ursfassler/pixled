/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled;

import java.util.ArrayList;
import java.util.List;

import ch.bitzgi.pixled.bluetooth.Adapter;
import ch.bitzgi.pixled.connection.ConnectionManager;
import ch.bitzgi.pixled.divoom.aurabox.AuraBox;
import ch.bitzgi.pixled.filesystem.Directory;
import ch.bitzgi.pixled.image.ImageLoader;
import ch.bitzgi.pixled.pixlet.PixletRepository;
import ch.bitzgi.pixled.pixlet.aurabox.screen.AuraboxScreens;
import ch.bitzgi.pixled.pixlet.image.ShowImages;

public class PixledFactory {
    static public Pixled produce(Adapter adapter, Directory root, ImageLoader loader) {
        ConnectionManager connectionManager = new ConnectionManager(adapter);
        DivoomSender sender = new DivoomSender(connectionManager, adapter);
        AuraBox auraBox = new AuraBox(sender);
        List<PixletRepository> pixlets = new ArrayList<PixletRepository>();
        pixlets.add(new AuraboxScreens(auraBox, loader, root));
        pixlets.add(new ShowImages(root, loader, auraBox));
        return new Pixled(connectionManager, auraBox, pixlets);
    }

}