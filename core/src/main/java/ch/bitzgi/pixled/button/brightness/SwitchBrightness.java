/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.button.brightness;

import java.util.HashMap;
import java.util.Map;

import ch.bitzgi.pixled.divoom.aurabox.AuraBox;
import ch.bitzgi.pixled.divoom.aurabox.Brightness;
import ch.bitzgi.pixled.widget.button.ButtonActionHandler;

class SwitchBrightness implements ButtonActionHandler {
    private final AuraBox auraBox;
    private final BrightnessListener listener;
    private Brightness brightness;
    private final Map<Brightness, Brightness> NextBrightness;

    SwitchBrightness(AuraBox auraBox, BrightnessListener listener) {
        this.auraBox = auraBox;
        this.listener = listener;

        NextBrightness = new HashMap<Brightness, Brightness>();
        NextBrightness.put(Brightness.Light, Brightness.Dark);
        NextBrightness.put(Brightness.Dark, Brightness.Off);
        NextBrightness.put(Brightness.Off, Brightness.Light);
        assert NextBrightness.size() == Brightness.values().length;
    }

    @Override
    public void click() {
        setBrightness(NextBrightness.get(brightness));
    }

    public void reset() {
        setBrightness(Brightness.Light);
    }

    private void setBrightness(Brightness value) {
        if (brightness != value) {
            brightness = value;
            auraBox.setBrightness(brightness);
            listener.brightnessChanged(brightness);
        }
    }

}
