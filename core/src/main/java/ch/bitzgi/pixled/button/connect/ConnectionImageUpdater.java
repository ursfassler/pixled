/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.button.connect;

import java.util.HashMap;
import java.util.Map;

import ch.bitzgi.pixled.connection.ConnectionManagerListener;
import ch.bitzgi.pixled.connection.ConnectionState;
import ch.bitzgi.pixled.widget.image.SimpleFileImage;

class ConnectionImageUpdater implements ConnectionManagerListener {
    private final static Map<ConnectionState, String> ConnectionStateImage = createConnectionStateImage();
    private final SimpleFileImage image;

    ConnectionImageUpdater(SimpleFileImage image) {
        this.image = image;
    }

    @Override
    public void connectionStateChanged(ConnectionState state) {
        String name = ConnectionStateImage.get(state);
        image.setImage(name);
    }

    private static Map<ConnectionState, String> createConnectionStateImage() {
        Map<ConnectionState, String> images = new HashMap<ConnectionState, String>();

        images.put(ConnectionState.Disconnected, "disconnected");
        images.put(ConnectionState.Connecting, "connecting");
        images.put(ConnectionState.Connected, "connected");

        assert (images.size() == ConnectionState.values().length);

        return images;
    }

}