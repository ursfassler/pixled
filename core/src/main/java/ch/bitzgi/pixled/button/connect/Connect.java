/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.button.connect;

import ch.bitzgi.pixled.connection.ConnectionManager;
import ch.bitzgi.pixled.widget.button.ButtonActionHandler;

class Connect implements ButtonActionHandler {
    private final ConnectionManager manager;

    Connect(ConnectionManager manager) {
        this.manager = manager;
    }

    @Override
    public void click() {
        manager.connect();
    }

}
