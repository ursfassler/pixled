/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.button.brightness;

import ch.bitzgi.pixled.connection.ConnectionManagerListener;
import ch.bitzgi.pixled.connection.ConnectionState;

class OnConnectionReseter implements ConnectionManagerListener {
    private final SwitchBrightness switchBrightness;

    OnConnectionReseter(SwitchBrightness switchBrightness) {
        this.switchBrightness = switchBrightness;
    }

    @Override
    public void connectionStateChanged(ConnectionState state) {
        if (state == ConnectionState.Connected) {
            switchBrightness.reset();
        }
    }
}