/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.button.brightness;

import ch.bitzgi.pixled.connection.ConnectionManagerListener;
import ch.bitzgi.pixled.connection.ConnectionState;
import ch.bitzgi.pixled.widget.button.SimpleButton;

class OnConnectionEnabler implements ConnectionManagerListener {
    private final SimpleButton button;

    OnConnectionEnabler(SimpleButton button) {
        this.button = button;
    }

    @Override
    public void connectionStateChanged(ConnectionState state) {
        button.setEnabled(state == ConnectionState.Connected);
    }
}