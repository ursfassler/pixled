/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.button.connect;

import ch.bitzgi.pixled.connection.ConnectionManager;
import ch.bitzgi.pixled.connection.ConnectionManagerListener;
import ch.bitzgi.pixled.widget.SystemButton;
import ch.bitzgi.pixled.widget.button.SimpleButton;
import ch.bitzgi.pixled.widget.image.SimpleFileImage;

public class ConnectionButtonFactory {
    static public SystemButton produce(ConnectionManager connectionManager) {
        SimpleFileImage image = new SimpleFileImage();
        SimpleButton button = new SimpleButton();
        button.setListener(new Connect(connectionManager));
        addConnectionListener(connectionManager, new ConnectionImageUpdater(image));
        return new SystemButton("connection", image, button);
    }

    static private void addConnectionListener(ConnectionManager connectionManager, ConnectionManagerListener value) {
        connectionManager.addListener(value);
        value.connectionStateChanged(connectionManager.getState());
    }

}