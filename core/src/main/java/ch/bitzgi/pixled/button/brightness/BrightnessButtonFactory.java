/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.button.brightness;

import ch.bitzgi.pixled.connection.ConnectionManager;
import ch.bitzgi.pixled.connection.ConnectionManagerListener;
import ch.bitzgi.pixled.divoom.aurabox.AuraBox;
import ch.bitzgi.pixled.widget.SystemButton;
import ch.bitzgi.pixled.widget.button.SimpleButton;
import ch.bitzgi.pixled.widget.image.SimpleFileImage;

public class BrightnessButtonFactory {
    static public SystemButton produce(AuraBox auraBox, ConnectionManager connectionManager) {
        SimpleFileImage image = new SimpleFileImage();
        SimpleButton brightness = new SimpleButton();
        BrightnessImage brightnessImage = new BrightnessImage(image);
        SwitchBrightness switchBrightness = new SwitchBrightness(auraBox, brightnessImage);
        brightness.setListener(switchBrightness);
        switchBrightness.reset();
        addConnectionListener(connectionManager, new OnConnectionReseter(switchBrightness));
        addConnectionListener(connectionManager, new OnConnectionEnabler(brightness));
        return new SystemButton("brightness", image, brightness);
    }

    static private void addConnectionListener(ConnectionManager connectionManager, ConnectionManagerListener value) {
        connectionManager.addListener(value);
        value.connectionStateChanged(connectionManager.getState());
    }

}