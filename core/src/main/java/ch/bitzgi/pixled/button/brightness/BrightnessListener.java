/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.button.brightness;

import ch.bitzgi.pixled.divoom.aurabox.Brightness;

interface BrightnessListener {

    void brightnessChanged(Brightness value);

}