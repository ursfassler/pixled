/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.button.brightness;

import java.util.HashMap;
import java.util.Map;

import ch.bitzgi.pixled.divoom.aurabox.Brightness;
import ch.bitzgi.pixled.widget.image.SimpleFileImage;

class BrightnessImage implements BrightnessListener {
    private final SimpleFileImage brightness;
    private final Map<Brightness, String> BrightnessImage;

    BrightnessImage(SimpleFileImage brightness) {
        this.brightness = brightness;

        BrightnessImage = new HashMap<Brightness, String>();
        BrightnessImage.put(Brightness.Off, "brightnessOff");
        BrightnessImage.put(Brightness.Dark, "brightnessDark");
        BrightnessImage.put(Brightness.Light, "brightnessLight");
        assert BrightnessImage.size() == Brightness.values().length;
    }

    @Override
    public void brightnessChanged(Brightness value) {
        String image = BrightnessImage.get(value);
        brightness.setImage(image);
    }

}