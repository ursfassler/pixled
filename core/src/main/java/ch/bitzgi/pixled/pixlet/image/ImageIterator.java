/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.image;

import ch.bitzgi.pixled.image.Image;

interface ImageIterator {
    void next();

    Image image();
}