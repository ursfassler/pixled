/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.image;

import java.util.List;

import ch.bitzgi.pixled.image.Color;
import ch.bitzgi.pixled.image.ColorSeries;
import ch.bitzgi.pixled.image.Image;

class ColorizableImage implements ImageIterator {
    private final Image image;
    private final ColorSeries colors;

    ColorizableImage(Image image, ColorSeries colors) {
        this.image = image;
        this.colors = colors;
    }

    @Override
    public void next() {
        colors.next();
    }

    @Override
    public Image image() {
        List<Color> pixels = Recolorize.colorize(image.pixels, colors.current());
        return new Image(image.width, image.height, pixels);
    }
}