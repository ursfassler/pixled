/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.aurabox.screen;

import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.pixlet.PixletListener;

class NoChange implements ChangeFunction {
    private final Image image;

    public NoChange(Image image) {
        this.image = image;
    }

    @Override
    public void change() {
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public void setListener(PixletListener listener) {
    }

}