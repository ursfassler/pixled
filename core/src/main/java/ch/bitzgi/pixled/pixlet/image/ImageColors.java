/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */
package ch.bitzgi.pixled.pixlet.image;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.bitzgi.pixled.image.Color;

class ImageColors {

    static boolean hasOneColor(List<Color> pixels) {
        Set<Color> colors = new HashSet<Color>(pixels);
        colors.remove(Color.BLACK);
        return colors.size() == 1;
    }

    public static Color getSingleColor(List<Color> pixels) {
        for (Color color : pixels) {
            if (!color.equals(Color.BLACK)) {
                return color;
            }
        }
        return Color.BLACK;
    }

}
