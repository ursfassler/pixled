/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.image;

import java.util.ArrayList;
import java.util.List;

import ch.bitzgi.pixled.image.Color;

class Recolorize {

    public static List<Color> colorize(List<Color> pixels, Color color) {
        List<Color> result = new ArrayList<Color>();

        for (Color pixel : pixels) {
            Color newColor = pixel.equals(Color.BLACK) ? Color.BLACK : color;
            result.add(newColor);
        }

        return result;
    }

}
