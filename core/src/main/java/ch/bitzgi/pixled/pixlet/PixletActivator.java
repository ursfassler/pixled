/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet;

public class PixletActivator {
    private Pixlet active;

    public void clicked(Pixlet pixlet) {
        if (active == pixlet) {
            pixlet.change();
        } else {
            active = pixlet;
            pixlet.activate();
        }
    }

}
