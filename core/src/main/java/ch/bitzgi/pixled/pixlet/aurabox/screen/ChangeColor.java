/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.aurabox.screen;

import java.util.ArrayList;
import java.util.List;

import ch.bitzgi.pixled.divoom.aurabox.AuraBox;
import ch.bitzgi.pixled.image.Color;
import ch.bitzgi.pixled.image.ColorSeries;
import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.pixlet.NullPixletListener;
import ch.bitzgi.pixled.pixlet.PixletListener;

class ChangeColor implements ChangeFunction {
    private Image image;
    private PixletListener listener = new NullPixletListener();
    private final AuraBox auraBox;
    private final ColorSeries colorSeries = new ColorSeries(Color.YELLOW);

    ChangeColor(AuraBox auraBox) {
        image = creatImage(colorSeries.current(), auraBox.getWidth(), auraBox.getHeight());
        this.auraBox = auraBox;
    }

    @Override
    public void change() {
        colorSeries.next();

        Color color = colorSeries.current();
        auraBox.setColor(color);

        image = creatImage(color, auraBox.getWidth(), auraBox.getHeight());
        listener.imageChanged();
    }

    private Image creatImage(Color color, int width, int height) {
        int size = width * height;
        List<Color> pixels = new ArrayList<Color>(size);
        for (int i = 0; i < size; i++) {
            pixels.add(color);
        }

        return new Image(width, height, pixels);
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public void setListener(PixletListener listener) {
        this.listener = listener;
    }

}