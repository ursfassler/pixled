/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet;

import java.util.List;

public interface PixletRepository {

    List<Pixlet> all();

}
