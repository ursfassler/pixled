/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet;

import ch.bitzgi.pixled.image.Image;

public interface Pixlet {

    String getName();

    Image getImage();

    void activate();

    void change();

    void setListener(PixletListener listener);

}