/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet;

public class NullPixletListener implements PixletListener {
    @Override
    public void imageChanged() {
    }

}