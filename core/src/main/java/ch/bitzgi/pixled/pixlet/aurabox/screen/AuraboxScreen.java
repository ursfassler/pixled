/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.aurabox.screen;

import ch.bitzgi.pixled.divoom.aurabox.AuraBox;
import ch.bitzgi.pixled.divoom.aurabox.Screen;
import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.pixlet.Pixlet;
import ch.bitzgi.pixled.pixlet.PixletListener;

class AuraboxScreen implements Pixlet {
    private final Screen screen;
    private final ChangeFunction changeFunction;
    private final AuraBox auraBox;

    AuraboxScreen(Screen screen, ChangeFunction changeFunction, AuraBox auraBox) {
        this.screen = screen;
        this.changeFunction = changeFunction;
        this.auraBox = auraBox;
    }

    @Override
    public void activate() {
        auraBox.showScreen(screen);
    }

    @Override
    public void change() {
        changeFunction.change();
    }

    @Override
    public String getName() {
        return "switchTo" + screen.toString();
    }

    @Override
    public Image getImage() {
        return changeFunction.getImage();
    }

    @Override
    public void setListener(PixletListener listener) {
        changeFunction.setListener(listener);
    }

}