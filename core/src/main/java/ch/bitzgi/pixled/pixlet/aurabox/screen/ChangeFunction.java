/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.aurabox.screen;

import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.pixlet.PixletListener;

interface ChangeFunction {

    void change();

    Image getImage();

    void setListener(PixletListener listener);

}