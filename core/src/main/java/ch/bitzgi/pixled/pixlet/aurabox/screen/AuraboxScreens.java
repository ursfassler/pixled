/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.aurabox.screen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.bitzgi.pixled.divoom.aurabox.AuraBox;
import ch.bitzgi.pixled.divoom.aurabox.Screen;
import ch.bitzgi.pixled.divoom.aurabox.TemparatureUnit;
import ch.bitzgi.pixled.filesystem.Directory;
import ch.bitzgi.pixled.filesystem.File;
import ch.bitzgi.pixled.filesystem.FileSystem;
import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.image.ImageLoader;
import ch.bitzgi.pixled.pixlet.Pixlet;
import ch.bitzgi.pixled.pixlet.PixletRepository;

public class AuraboxScreens implements PixletRepository {
    private final AuraBox auraBox;
    private final ImageLoader loader;
    private final Directory root;

    public AuraboxScreens(AuraBox auraBox, ImageLoader loader, Directory root) {
        this.auraBox = auraBox;
        this.loader = loader;
        this.root = root;
    }

    @Override
    public List<Pixlet> all() {
        List<Pixlet> result = new ArrayList<Pixlet>();

        for (Screen screen : Screen.values()) {
            ChangeFunction change = createChangeFunction(screen);
            AuraboxScreen pixlet = new AuraboxScreen(screen, change, auraBox);
            result.add(pixlet);
        }

        return result;
    }

    private ChangeFunction createChangeFunction(Screen screen) {
        switch (screen) {
        case Temperature: {
            Map<TemparatureUnit, Image> images = getTemperatureUnitImages(screen.toString() + "-");
            return new ChangeTemparatureUnit(images, auraBox);
        }
        case Light: {
            return new ChangeColor(auraBox);
        }
        default: {
            Image image = getImage(screen.toString());
            return new NoChange(image);
        }
        }
    }

    private Map<TemparatureUnit, Image> getTemperatureUnitImages(String prefix) {
        Map<TemparatureUnit, Image> images = new HashMap<TemparatureUnit, Image>();
        for (TemparatureUnit unit : TemparatureUnit.values()) {
            Image image = getImage(prefix + unit.toString());
            images.put(unit, image);
        }
        return images;
    }

    private Image getImage(String name) {
        File file = findImage(name);
        if (file == null) {
            return new Image();
        }
        return loader.load(file);
    }

    private File findImage(String name) {
        Directory images = FileSystem.findDirectory(root, "screen");
        if (images == null) {
            return null;
        }
        return FileSystem.findFile(images, name, loader.supportedExtensions());
    }

}