/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.aurabox.screen;

import java.util.HashMap;
import java.util.Map;

import ch.bitzgi.pixled.divoom.aurabox.AuraBox;
import ch.bitzgi.pixled.divoom.aurabox.TemparatureUnit;
import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.pixlet.NullPixletListener;
import ch.bitzgi.pixled.pixlet.PixletListener;

class ChangeTemparatureUnit implements ChangeFunction {
    private final Map<TemparatureUnit, Image> images;
    private PixletListener listener = new NullPixletListener();
    private final AuraBox auraBox;
    private TemparatureUnit currentTemperatureUnit = TemparatureUnit.Celsius;

    ChangeTemparatureUnit(Map<TemparatureUnit, Image> image, AuraBox auraBox) {
        this.images = new HashMap<TemparatureUnit, Image>(image);
        this.auraBox = auraBox;
    }

    @Override
    public void change() {
        int oldIndex = currentTemperatureUnit.ordinal();
        int newIndex = (oldIndex + 1) % TemparatureUnit.values().length;
        currentTemperatureUnit = TemparatureUnit.values()[newIndex];

        auraBox.setTemparatureUnit(currentTemperatureUnit);
        listener.imageChanged();
    }

    @Override
    public Image getImage() {
        return images.get(currentTemperatureUnit);
    }

    @Override
    public void setListener(PixletListener listener) {
        this.listener = listener;
    }

}