/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.image;

import ch.bitzgi.pixled.divoom.aurabox.AuraBox;
import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.pixlet.NullPixletListener;
import ch.bitzgi.pixled.pixlet.Pixlet;
import ch.bitzgi.pixled.pixlet.PixletListener;

class ShowImage implements Pixlet {
    private final String name;
    private final AuraBox auraBox;
    private final ImageIterator image;
    private PixletListener listener = new NullPixletListener();

    public ShowImage(String name, AuraBox auraBox, ImageIterator image) {
        this.name = name;
        this.auraBox = auraBox;
        this.image = image;
    }

    @Override
    public void activate() {
        auraBox.showImage(image.image());
    }

    @Override
    public void change() {
        image.next();
        listener.imageChanged();
        auraBox.showImage(image.image());
    }

    @Override
    public String getName() {
        return "image " + name;
    }

    @Override
    public Image getImage() {
        return image.image();
    }

    @Override
    public void setListener(PixletListener listener) {
        this.listener = listener;
    }

}