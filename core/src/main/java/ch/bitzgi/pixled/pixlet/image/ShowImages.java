/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.image;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ch.bitzgi.pixled.divoom.aurabox.AuraBox;
import ch.bitzgi.pixled.filesystem.Directory;
import ch.bitzgi.pixled.filesystem.File;
import ch.bitzgi.pixled.filesystem.FileSystem;
import ch.bitzgi.pixled.image.ColorSeries;
import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.image.ImageLoader;
import ch.bitzgi.pixled.pixlet.Pixlet;
import ch.bitzgi.pixled.pixlet.PixletRepository;

public class ShowImages implements PixletRepository {
    private final Directory root;
    private final ImageLoader loader;
    private final AuraBox auraBox;

    public ShowImages(Directory root, ImageLoader loader, AuraBox auraBox) {
        this.root = root;
        this.loader = loader;
        this.auraBox = auraBox;
    }

    @Override
    public List<Pixlet> all() {
        Directory images = FileSystem.findDirectory(root, "images");
        if (images == null) {
            return new ArrayList<Pixlet>();
        }
        return loadImages(images);
    }

    private List<Pixlet> loadImages(Directory images) {
        List<Pixlet> result = new ArrayList<Pixlet>();
        List<File> files = sort(images.listFiles());

        for (File file : files) {
            Image image = loader.load(file);
            if (isUsable(image)) {
                String filename = FileSystem.path(file);
                ShowImage pixlet = create(image, filename);
                result.add(pixlet);
            }
        }
        return result;
    }

    private boolean isUsable(Image image) {
        return (image.width == 10) && (image.height == 10);
    }

    private List<File> sort(Collection<? extends File> value) {
        List<File> result = new ArrayList<File>();
        result.addAll(value);
        Collections.sort(result, new Comparator<File>() {
            @Override
            public int compare(File left, File right) {
                return left.name().compareTo(right.name());
            }
        });
        return result;
    }

    private ShowImage create(Image image, String filename) {
        String name = getImageName(filename);
        boolean singleColored = ImageColors.hasOneColor(image.pixels);
        ImageIterator imageItr = singleColored ? createColorizable(image) : createStatic(image);
        return new ShowImage(name, auraBox, imageItr);
    }

    private String getImageName(String filename) {
        int last = filename.lastIndexOf('.');
        String name = filename.substring(0, last);
        return name;
    }

    private ImageIterator createStatic(Image image) {
        return new StaticImage(image);
    }

    private ImageIterator createColorizable(Image image) {
        ColorSeries colorSeries = new ColorSeries(ImageColors.getSingleColor(image.pixels));
        return new ColorizableImage(image, colorSeries);
    }

}