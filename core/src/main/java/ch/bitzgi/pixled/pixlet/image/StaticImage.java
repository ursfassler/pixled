/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.pixlet.image;

import ch.bitzgi.pixled.image.Image;

class StaticImage implements ImageIterator {
    private final Image image;

    StaticImage(Image image) {
        this.image = image;
    }

    @Override
    public void next() {
    }

    @Override
    public Image image() {
        return image;
    }
}