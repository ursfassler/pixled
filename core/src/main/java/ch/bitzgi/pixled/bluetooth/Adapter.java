/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.bluetooth;

import java.util.List;

public interface Adapter {

    void connectTo(Device device, ConnectionListener listener);

    List<Device> getDevices();

    void send(List<Byte> data);

}
