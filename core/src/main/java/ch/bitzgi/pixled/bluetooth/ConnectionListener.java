/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.bluetooth;

import java.util.List;

public interface ConnectionListener {
    void connected(Device device);

    void disconnected();

    void received(List<Byte> data);

}
