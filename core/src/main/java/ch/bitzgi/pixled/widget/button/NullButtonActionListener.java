/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.widget.button;

public class NullButtonActionListener implements ButtonActionHandler {

    @Override
    public void click() {
    }

}
