/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.widget.image;

import ch.bitzgi.pixled.widget.ImageListener;

public class NullImageListener implements ImageListener {

    @Override
    public void imageChanged() {
    }

}
