/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.widget;

import ch.bitzgi.pixled.image.Image;

public interface MemoryImage extends ScreenImage {
    Image getImage();

}