/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.widget.button;

import ch.bitzgi.pixled.widget.ButtonStateListener;

public class NullButtonStateListener implements ButtonStateListener {

    @Override
    public void enabledChanged() {
    }

}
