/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.widget;

public class SystemButton {
    private final String name;
    private final FileImage image;
    private final Button button;

    public SystemButton(String name, FileImage image, Button button) {
        this.name = name;
        this.image = image;
        this.button = button;
    }

    public String getName() {
        return name;
    }

    public FileImage getImage() {
        return image;
    }

    public Button getButton() {
        return button;
    }

}
