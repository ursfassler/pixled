/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.widget;

public class PixletButton {
    private final String name;
    private final MemoryImage image;
    private final Button button;

    public PixletButton(String name, MemoryImage image, Button button) {
        this.name = name;
        this.image = image;
        this.button = button;
    }

    public String getName() {
        return name;
    }

    public MemoryImage getImage() {
        return image;
    }

    public Button getButton() {
        return button;
    }

}
