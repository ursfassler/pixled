/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.widget.image;

import java.util.ArrayList;

import ch.bitzgi.pixled.image.Color;
import ch.bitzgi.pixled.image.Image;
import ch.bitzgi.pixled.widget.ImageListener;
import ch.bitzgi.pixled.widget.MemoryImage;

public class SimpleMemoryImage implements MemoryImage {
    private Image image = new Image(0, 0, new ArrayList<Color>());
    private ImageListener listener = new NullImageListener();

    @Override
    public void setListener(ImageListener listener) {
        assert listener != null;
        this.listener = listener;
    }

    public void setImage(Image value) {
        if (!image.equals(value)) {
            image = value;
            listener.imageChanged();
        }
    }

    @Override
    public Image getImage() {
        return image;
    }

}