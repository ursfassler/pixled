/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.widget.image;

import ch.bitzgi.pixled.widget.FileImage;
import ch.bitzgi.pixled.widget.ImageListener;

public class SimpleFileImage implements FileImage {
    private String image = "";
    private ImageListener listener = new NullImageListener();

    @Override
    public void setListener(ImageListener listener) {
        assert listener != null;
        this.listener = listener;
    }

    public void setImage(String value) {
        if (!image.equals(value)) {
            image = value;
            listener.imageChanged();
        }
    }

    @Override
    public String getImage() {
        return image;
    }

}