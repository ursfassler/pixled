/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.widget;

public interface Button {
    void click();

    Boolean getEnabled();

    void setListener(ButtonStateListener listener);

}
