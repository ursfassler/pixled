/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.widget.button;

import ch.bitzgi.pixled.widget.Button;
import ch.bitzgi.pixled.widget.ButtonStateListener;

public class SimpleButton implements Button {
    private boolean enabled = true;
    private ButtonStateListener stateListener = new NullButtonStateListener();
    private ButtonActionHandler actionListener = new NullButtonActionListener();

    @Override
    public void setListener(ButtonStateListener listener) {
        assert listener != null;
        this.stateListener = listener;
    }

    public void setListener(ButtonActionHandler listener) {
        assert listener != null;
        this.actionListener = listener;
    }

    @Override
    public void click() {
        actionListener.click();
    }

    public void setEnabled(boolean value) {
        if (enabled != value) {
            enabled = value;
            stateListener.enabledChanged();
        }
    }

    @Override
    public Boolean getEnabled() {
        return enabled;
    }

}
