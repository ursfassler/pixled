/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled;

import java.util.List;

import ch.bitzgi.pixled.bluetooth.Adapter;
import ch.bitzgi.pixled.connection.ConnectionManager;
import ch.bitzgi.pixled.connection.ConnectionState;
import ch.bitzgi.pixled.divoom.ByteSender;
import ch.bitzgi.pixled.divoom.FrameEncoder;
import ch.bitzgi.pixled.divoom.MessageEncoder;

class DivoomSender implements ByteSender {
    private final ConnectionManager connectionManager;
    private final Adapter adapter;
    private final static FrameEncoder FRAME_ENCODER = new FrameEncoder();
    private final static MessageEncoder MESSAGE_ENCODER = new MessageEncoder();

    public DivoomSender(ConnectionManager connectionManager, Adapter adapter) {
        this.connectionManager = connectionManager;
        this.adapter = adapter;
    }

    @Override
    public void send(List<Byte> value) {
        if (connectionManager.getState() != ConnectionState.Connected) {
            return;
        }

        value = MESSAGE_ENCODER.encode(value);
        value = FRAME_ENCODER.encode(value);

        adapter.send(value);
    }

}