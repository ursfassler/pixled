/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled;

import ch.bitzgi.pixled.pixlet.Pixlet;
import ch.bitzgi.pixled.pixlet.PixletActivator;
import ch.bitzgi.pixled.widget.button.ButtonActionHandler;

class PixletActivatorAdapter implements ButtonActionHandler {
    private final Pixlet pixlet;
    private final PixletActivator activator;

    PixletActivatorAdapter(Pixlet pixlet, PixletActivator activator) {
        this.pixlet = pixlet;
        this.activator = activator;
    }

    @Override
    public void click() {
        activator.clicked(pixlet);
    }

}