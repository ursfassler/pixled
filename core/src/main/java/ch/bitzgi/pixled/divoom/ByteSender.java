/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.divoom;

import java.util.List;

public interface ByteSender {

    void send(List<Byte> value);

}