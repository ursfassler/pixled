/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.divoom;

import java.util.ArrayList;
import java.util.List;

public class MessageEncoder {
    public List<Byte> encode(List<Byte> message) {
        List<Byte> result = new ArrayList<Byte>();

        result.addAll(encode16(message.size() + 2));
        result.addAll(message);

        int sum = 0;
        for (byte sym : result) {
            sum += ByteList.asInt(sym);
        }
        result.addAll(encode16(sum));

        return result;
    }

    private List<Byte> encode16(int value) {
        List<Byte> result = new ArrayList<Byte>();

        result.add(ByteList.asByte(value & 0xff));
        result.add(ByteList.asByte(value >> 8));

        return result;
    }
}