/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.divoom;

import java.util.ArrayList;
import java.util.List;

public class ByteList {
    public static byte asByte(int value) {
        return (byte) value;
    }

    public static int asInt(byte value) {
        return value >= 0 ? value : (0x100 + value);
    }

    public static List<Byte> bytes(int a0) {
        List<Byte> result = new ArrayList<Byte>();
        result.add(asByte(a0));
        return result;
    }

    public static List<Byte> bytes(int a0, int a1) {
        List<Byte> result = bytes(a0);
        result.add(asByte(a1));
        return result;
    }

}