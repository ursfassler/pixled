/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.divoom.aurabox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.bitzgi.pixled.divoom.ByteList;
import ch.bitzgi.pixled.divoom.ByteSender;
import ch.bitzgi.pixled.image.Color;
import ch.bitzgi.pixled.image.Image;

public class AuraBox {
    private static final Map<Screen, Integer> ScreenValue = createScreenValue();
    private static final Map<Brightness, Integer> BrightnessValue = createBrightnessValue();
    private static final Map<TemparatureUnit, Integer> TemparatureUnitValue = createTemperatureUnitValue();

    private final ByteSender sender;

    public AuraBox(ByteSender sender) {
        this.sender = sender;
    }

    public void showScreen(Screen value) {
        int screenNumber = ScreenValue.get(value);
        sender.send(ByteList.bytes(0x45, screenNumber));
    }

    public void setBrightness(Brightness value) {
        int brightness = BrightnessValue.get(value);
        sender.send(ByteList.bytes(0x32, brightness));
    }

    public void showImage(Image value) {
        assert value.width == 10;
        assert value.height == 10;

        List<Byte> command = new ArrayList<Byte>();
        command.add((byte) 0x44);
        command.add((byte) 0x00);
        command.add((byte) 0x0a);
        command.add((byte) 0x0a);
        command.add((byte) 0x04);

        for (int i = 0; i < ((10 * 10) / 2); i++) {
            int lower = ColorConverter.to3Bit(value.pixels.get(i * 2));
            int higher = ColorConverter.to3Bit(value.pixels.get(i * 2 + 1));
            int combined = (higher << 4) | lower;
            command.add((byte) combined);
        }

        sender.send(command);
    }

    public void setTemparatureUnit(TemparatureUnit value) {
        int temparatureUnit = TemparatureUnitValue.get(value);
        sender.send(ByteList.bytes(0x4c, temparatureUnit));

    }

    public void setColor(Color value) {
        int color = ColorConverter.to3Bit(value);
        sender.send(ByteList.bytes(0x47, color));
    }

    private static Map<Screen, Integer> createScreenValue() {
        Map<Screen, Integer> value = new HashMap<Screen, Integer>();

        value.put(Screen.Time, 0);
        value.put(Screen.Temperature, 1);
        value.put(Screen.Light, 2);
        value.put(Screen.Equalizer, 3);
        value.put(Screen.Imagination, 4);
        value.put(Screen.Party, 5);
        value.put(Screen.Rainbow, 6);
        value.put(Screen.Explosion, 7);

        assert value.size() == Screen.values().length;
        return value;
    }

    private static Map<Brightness, Integer> createBrightnessValue() {
        Map<Brightness, Integer> value = new HashMap<Brightness, Integer>();

        value.put(Brightness.Off, 0x00);
        value.put(Brightness.Dark, 0x3f);
        value.put(Brightness.Light, 0xd2);

        assert value.size() == Brightness.values().length;
        return value;
    }

    private static HashMap<TemparatureUnit, Integer> createTemperatureUnitValue() {
        HashMap<TemparatureUnit, Integer> value = new HashMap<TemparatureUnit, Integer>();

        value.put(TemparatureUnit.Celsius, 0x00);
        value.put(TemparatureUnit.Farenheit, 0x01);

        assert value.size() == TemparatureUnit.values().length;
        return value;
    }

    public int getWidth() {
        return 10;
    }

    public int getHeight() {
        return 10;
    }

}