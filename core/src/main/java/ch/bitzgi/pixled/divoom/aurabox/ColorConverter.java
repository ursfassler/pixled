/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.divoom.aurabox;

import ch.bitzgi.pixled.image.Color;

class ColorConverter {

    public static int to3Bit(Color value) {
        int red = (value.value >> (16 + 7)) & 0x1;
        int green = (value.value >> (8 + 7)) & 0x1;
        int blue = (value.value >> (0 + 7)) & 0x1;
        int color = (red << 0) | (green << 1) | (blue << 2);
        return color;
    }

}
