/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.divoom.aurabox;

public enum Screen {
    Time, Temperature, Light, Equalizer, Imagination, Party, Rainbow, Explosion

}
