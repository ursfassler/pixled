/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled.divoom;

import java.util.ArrayList;
import java.util.List;

public class FrameEncoder {
    private final static byte START_OF_FRAME = 0x01;
    private final static byte END_OF_FRAME = 0x02;
    private final static byte ESCAPE_CHARACTER = 0x03;
    private final static byte ESCAPE_OFFSET = 0x03;

    public List<Byte> encode(List<Byte> message) {
        List<Byte> result = new ArrayList<Byte>();

        result.add(START_OF_FRAME);
        for (byte sym : message) {
            result.addAll(encode(sym));
        }
        result.add(END_OF_FRAME);

        return result;
    }

    private List<Byte> encode(byte symbol) {
        switch (symbol) {
        case START_OF_FRAME:
        case END_OF_FRAME:
        case ESCAPE_CHARACTER:
            return ByteList.bytes(ESCAPE_CHARACTER, ESCAPE_OFFSET + symbol);

        default:
            return ByteList.bytes(symbol);
        }
    }

}