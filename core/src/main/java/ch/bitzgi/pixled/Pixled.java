/*
 * Copyright 2019 Urs Fässler
 * SPDX-License-Identifier: GPL-3.0-only
 */

package ch.bitzgi.pixled;

import java.util.ArrayList;
import java.util.List;

import ch.bitzgi.pixled.button.brightness.BrightnessButtonFactory;
import ch.bitzgi.pixled.button.connect.ConnectionButtonFactory;
import ch.bitzgi.pixled.connection.ConnectionManager;
import ch.bitzgi.pixled.divoom.aurabox.AuraBox;
import ch.bitzgi.pixled.pixlet.Pixlet;
import ch.bitzgi.pixled.pixlet.PixletActivator;
import ch.bitzgi.pixled.pixlet.PixletListener;
import ch.bitzgi.pixled.pixlet.PixletRepository;
import ch.bitzgi.pixled.widget.PixletButton;
import ch.bitzgi.pixled.widget.SystemButton;
import ch.bitzgi.pixled.widget.button.SimpleButton;
import ch.bitzgi.pixled.widget.image.SimpleMemoryImage;

public class Pixled {
    private final ConnectionManager connectionManager;
    private final AuraBox auraBox;
    private final List<PixletRepository> pixlets;
    private final PixletActivator activator = new PixletActivator();
    private final List<SystemButton> system = new ArrayList<SystemButton>();
    private final List<PixletButton> content = new ArrayList<PixletButton>();

    public Pixled(ConnectionManager connectionManager, AuraBox auraBox, List<PixletRepository> pixlets) {
        this.connectionManager = connectionManager;
        this.auraBox = auraBox;
        this.pixlets = pixlets;
    }

    public void boot() {
        system.add(ConnectionButtonFactory.produce(connectionManager));
        system.add(BrightnessButtonFactory.produce(auraBox, connectionManager));

        for (PixletRepository repository : pixlets) {
            for (Pixlet pixlet : repository.all()) {
                content.add(createPixletButton(pixlet));
            }
        }

        connectionManager.connect();
    }

    private PixletButton createPixletButton(Pixlet pixlet) {
        SimpleMemoryImage image = new SimpleMemoryImage();
        SimpleButton button = new SimpleButton();
        image.setImage(pixlet.getImage());
        button.setListener(new PixletActivatorAdapter(pixlet, activator));
        pixlet.setListener(new ImageUpdater(image, pixlet));
        return new PixletButton(pixlet.getName(), image, button);
    }

    public List<SystemButton> getSystem() {
        return system;
    }

    public List<PixletButton> getContent() {
        return content;
    }

}

class ImageUpdater implements PixletListener {
    private final SimpleMemoryImage image;
    private final Pixlet pixlet;

    public ImageUpdater(SimpleMemoryImage image, Pixlet pixlet) {
        this.image = image;
        this.pixlet = pixlet;
    }

    @Override
    public void imageChanged() {
        image.setImage(pixlet.getImage());
    }
}
